# Jack's Blog

[![Build Status](https://ci.cubieserver.de/api/badges/jh/blog/status.svg)](https://ci.cubieserver.de/jh/blog)

Source controlled and built by [Gitea](https://gitea.io/), Rendered by [Hugo](https://gohugo.io), Stored in [Minio](https://minio.io/), Cached by [Nginx](http://nginx.org/), Served by [Traefik](https://traefik.io/).

## Building

Run the `./testit.sh` script in the main directory to get a live preview served at <http://localhost:1313> (requires `podman` to be available).

The CI/CD server runs a build on each push: pushes to the `master` branch are published at <https://blog.cubieserver.de>, pushes on all other branches go to <https://test-blog.cubieserver.de> (these will overwrite each other, latest push wins).

To skip a CI build (i.e. not run an integration test for this specific commit, e.g. because it doesn't modify any content), use `[CI SKIP]` in the commit message.

## Images

Command for compressing and optimizing header background image (from <https://dev.to/feldroy/til-strategies-for-compressing-jpg-files-with-imagemagick-5fn9>):
```
convert orig.jpg \
-resize 50% \
-sampling-factor 4:2:0 \
-strip \
-quality 90 \
-interlace JPEG \
-colorspace RGB \
compressed.jpg
```

Create JPG thumbnail ("poster") for video (from <https://stackoverflow.com/a/68705261>):
```
ffmpeg -ss 00:01:00 -i $video -vf 'scale=1280:720' -vframes 1 ${video}.jpg
```

Social Share Preview:

* <https://socialsharepreview.com/?url=https://blog.cubieserver.de>
* <https://www.opengraph.xyz/url/https%3A%2F%2Fblog.cubieserver.de>

## Videos

Trim / cut video from start timestamp (`ss`) to timestamp (`to`) (from <https://stackoverflow.com/a/42827058>):

```
ffmpeg -ss 00:01:00 -to 00:02:00 -i input.mp4 -c copy output.mp4
```
