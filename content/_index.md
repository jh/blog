+++
description = "Homepage"
aliases = [ "/about/", ] # preserve location of previous about page
shareimage = "jack-presenting.jpg"
+++

<style>
 .flex-container {
   display: flex; flex-grow: 1; flex-wrap: wrap;
 }
 .flex-item {
   flex-grow: 1;
 }
 .flex-item ul {
   margin-bottom: 0.4em;
   margin-top: 0.4em;
 }
 #avatar:hover { transform: scale(1.2); }
</style>

<br>

<div class="flex-container" style="align-items: center;">
  <div class="flex-item" style="flex: 1;">
    <a href="jack-presenting.jpg">
      <image id="avatar" src="jack-presenting.jpg" alt="Jack holding a presentation" style="object-fit: contain; border-radius: 50%; width: 100%; transition: transform 0.5s;"/>
    </a>
  </div>
  <div class="flex-item" style="flex: 2; padding-left: 1.5em;">
    <h2>Hi, I'm Jack!</h3>
      <p>
        I'm a security and cloud computing engineer, tech geek and avid cyclist.
        I enjoy sharing experiences and knowledge with others by <a href"/tags/presentation/">speaking at conferences</a> and <a href="/categories/software/">blogging about software</a> as well as my <a href="/categories/outdoor/">outdoor adventures</a> &mdash; and some things in between.
      </p>
  </div>
</div>

<br>

## Latest articles

{{% recentposts %}}

<br>

<div class="flex-container">
  <div class="flex-item">
    <ul class="pager">
      <li><a href="{{< ref "post" >}}">Blog Archive</span></a></li>
    </ul>
  </div>
  <div class="flex-item">
    <ul class="pager">
      <li><a href="/tags/presentation/">Presentations</span></a></li>
    </ul>
  </div>
  <div class="flex-item">
    <ul class="pager">
      <li><a href="{{< ref "publications" >}}">Publications</span></a></li>
    </ul>
  </div>
</div>

<!--
Some inspiration:

https://robertovitillo.com/about
https://povilasv.me/about/
https://www.sebastian-daschner.com/about
https://erikbern.com/about.html
https://majorhayden.com/
https://alejandrosuarez.eu/

-->

## About Me

I currently live in Geneva, Switzerland, where I work at the [CERN research organization](https://home.cern) as a platform engineer on OpenShift Kubernetes clusters: I focus on building a secure, reliable and maintainable platform-as-a-service for our internal users.

My special areas of interest are systems performance, observability and efficiency.
Practically, I find myself between development and operations: I like writing software, but if there is already a piece of open-source software that implements it, I prefer integrating those components.
Naturally, I am a proponent of DevOps culture and have a strong inclication towards Infrastructure-as-Code and documentation.

In my spare time I enjoy cooking, cycling, hiking and practicing calisthenics.
From time-to-time I will also throw in a trail run to mix things up a bit.

I also run my own server for several applications: the blog you are reading right now, Nextcloud (File sharing and groupware), Gitea (Git hosting), ejabberd (XMPP chat) among others.
This hobby allows me to practice my skills and experiment with new technologies, such as Kubernetes, Pulumi, Prometheus and others.

I am an advocate of Free, Libre and Open-source software.

<figure style="margin-bottom: 1.5em;"><figcaption>
  <h4><i><center>
    Camping at lake Brombachsee, Germany (left).
    <br>
    Junction Hackathon at Aalto University, Finland (right).
  </center></i></h4>
</figcaption>
<img src="20200624_075451_min.jpg" style="object-fit: contain; width: 49%;" loading="lazy">
<img src="IMG_20191117_100335446_min.jpg" style="object-fit: contain; width: 49%;" loading="lazy">
</figure>

## Career

Between February and August 2021 I was a Thesis worker in the Security Solutions team of [Ericsson Finland](https://www.ericsson.com/en).
Apart from writing my thesis on the topic of [Dimensioning, Performance and Optimization of Cloud-native Applications](https://aaltodoc.aalto.fi/handle/123456789/109318), I was also engaged in the daily activities of software development and server administration.

From January until August 2020 I have been working at [Kodit.io](https://kodit.io/en/company/) as a Junior DevOps Engineer, where I focused on backend software development (Python) and CI/CD.

From 2015 to 2019 I have been working at [Fraunhofer-Allianz Vision](https://www.vision.fraunhofer.de/en.html) as a student assistant for administration of Linux systems.

In 2017 I have completed an internship at [Nokia Solutions and Networks](https://www.nokia.com/networks/), where I evaluated two performance monitoring frameworks (SystemTap and Intel PT).

## Education

In 2021, I completed my Master's degree in [Security and Cloud Computing](https://secclo.eu) at [Aalto University](https://www.aalto.fi/en) (Espoo, Finland) and [EURECOM](https://www.eurecom.fr) (Sophia-Antipolis, France).
My [Master's thesis](https://aaltodoc.aalto.fi/handle/123456789/109318) covered the topic of cloud-native applications with a particular focus on autoscaling.

For my Bachelor's degree I studied Information and Communication technology (a mix between electrical engineering and computer science) at [Friedrich-Alexander University](https://www.fau.eu) (Erlangen, Germany).

[Here](/publications/) you can find an overview of my publications.

{{< figure src="20210710_215827_min.jpg" title="Hiking in Nuuksio National Park, Finland." width="100%" >}}

## Elsewhere

You can also find me on [GitHub](https://github.com/jacksgt), [LinkedIn](https://www.linkedin.com/in/jack-henschel) and the [Fediverse](https://social.secclo.community/jack) -- check out the buttons below.
