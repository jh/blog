+++
title = "ArgoCD Health Checks for OPA rules"
description = "Kubernetes resources deployed by ArgoCD can use custom health checks for their status. In this post I'm showing how to add a health check for OPA policies."
categories = "Software"
tags = ["argocd", "openpolicyagent", "kubernetes"]
date = "2022-02-07"
+++

ArgoCD is commonly used to deploy and manage resources in Kubernetes clusters.
One nice feature of ArgoCD is that it will continuously monitor the status of your resources (unlike for example Helm, which just creates the resources).
For example, when your deployment fails to scale up to the desired number of replicas, ArgoCD will mark the Deployment as "Unhealthy".

ArgoCD has many resource health checks already built-in, but of course it cannot possibly cover the breadth of all resources available for Kubernetes.
Furthermore, even if ArgoCD knows how to check the status of the resources, there might be additional constraints or conditions that ArgoCD does not take into account.
One such example are policy rules for [OpenPolicyAgent](https://www.openpolicyagent.org/docs/latest/kubernetes-introduction/) (OPA), which are stored in ConfigMaps when using OPA with [kube-mgmt](https://github.com/open-policy-agent/kube-mgmt).
In this case, the ConfigMap is always "Healthy" as long as it exists.
But [OPA adds an annotation to the ConfigMap](https://www.openpolicyagent.org/docs/latest/kubernetes-debugging/) which indicates if it was able to successfully parse the policy stored in the ConfigMap.
Thus, I wanted to reflect this information in ArgoCD.

Why? Because it makes troubleshooting simpler and provides quicker feedback: it is much easier to identify an ArgoCD application which is in state "Unhealthy" (with a big red heart if you're using the UI) rather than go through all its associated ConfigMaps and check if the value of a particular annotation.

My first step was adding an invalid snippet into one of our OPA policies and deploying it to the cluster:

```
package kubernetes.admission
import data.kubernetes.storageclasses

=this{}is[]invalid
# ...
```

As this picture shows, ArgoCD still believes the resource is "Healthy" and therefore marks the entire application as "Healthy".

{{< figure src="broken-but-healthy.png" >}}

Next, I set out to write a [custom health check for ArgoCD](https://argo-cd.readthedocs.io/en/stable/operator-manual/health/).
ArgoCD health checks (and resource actions) are written in [Lua](https://www.lua.org/about.html), a lightweight scripting language.
If you are new to Lua (like me!), I recommend reading these short introductions for "Programming in Lua":
[Types and Values](https://www.lua.org/pil/2.html) as well as [Tables](https://www.lua.org/pil/2.5.html).
The [health checks already defined in the ArgoCD repo](https://github.com/argoproj/argo-cd/tree/master/resource_customizations) serve as good examples as well.

After a bit of trial-and-error, I came up with the following snippet:

```lua
hs = {}
hs.status = "Healthy"
local opa_annotation = "openpolicyagent.org/policy-status"
if obj.metadata.annotations ~= nil then
  if obj.metadata.annotations[opa_annotation] ~= nil then
    if obj.metadata.annotations[opa_annotation] == '{"status":"ok"}' then
      hs.status = "Healthy"
      hs.message = "Policy loaded successfully"
    else
      hs.status = "Degraded"
      hs.message = obj.metadata.annotations[opa_annotation]
    end
  end
end
return hs
```

`hs` is the health status object we will return to ArgoCD.
It must contain a `status` attribute which indicates whether the resource is `Healthy`, `Progressing`, `Degraded` or `Suspended`.
By default we set it to `Healthy`, since we don't want to mess with the status of other, non-OPA ConfigMaps.
Optionally, the health status object may also contain a `message`.

In lines 4-6 we identify if the ConfigMap is indeed an OPA policy or another kind of ConfigMap.
If it is a OPA policy, we retrieve the value of the `openpolicyagent.org/policy-status` annotation (line 7).
The annotation is set to `{"status":"ok"}` if the policy was loaded successfully, if errors occurred during loading (e.g., because the policy contained a syntax error) the cause will be reported in the annotation.
Depending on the value of the annotation, we set the `status` and `message` attributes appropriately.

At the end, we return the `hs` object to ArgoCD.

The only step left is telling ArgoCD about our custom health check.
This is done by adding the snippet about into the ArgoCD configuration in `argo-cm` ConfigMap:

```yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: argocd-cm
data:
resource.customizations.health.ConfigMap: |
  <our-snippet>
```

This health check will apply to all resources of type `ConfigMap`.
Note that usually you need to use the format `GROUP_RESOURCE` (like `argoproj.io_Application`), however since [core resources don't have a group only the resource name is used an identifier](https://github.com/argoproj/argo-cd/blob/ac1254017f5ae968ad0919efcca537fc6b732da7/util/lua/lua.go#L329).

If you are using the [ArgoCD Helm chart](https://github.com/argoproj/argo-helm) you can directly inject the snippet into the [`argo-cd.server.config` value](https://github.com/argoproj/argo-helm/blob/master/charts/argo-cd/values.yaml#L1139).

After ArgoCD reloads its configuration, it shows our OPA policy as "Unhealthy"!

{{< figure src="unhealthy-app.png" >}}

In addition, we get a nice error message in ArgoCD which immediately tells us what's wrong with the resource:

{{< figure src="unhealthy-error.png" >}}

Happy (and healthy!) deploying!
