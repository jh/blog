+++
title = "New Bike Day: BMC Teammachine SLR FOUR 2023"
description = "A quick look and review of my new bicycle: the BMC Teammachine SLR FOUR Model 2023."
categories = "outdoor"
tags = ["cycling"]
date = "2022-08-17"
image = "full-bike.avif"
+++

Since the beginning of this year, I have been looking for a new bicycle that I can use to *properly* explore the mountains and valleys of Switzerland and neighboring countries.
Despite actively checking the second-hand market for quite a few months, nothing suitable turned up there - especially not for my size.
Similarly, the supply chain situation on the new-bike market is still unbelievable: for any new bike that you find, you either get *"Out of Stock"*, *"Delivery in 20 weeks"* or *"Email when available"*
-- I'm honestly not sure which of these options is worse.

Thus, I was quite lucky that a local bike shop just got delivery of a new BMC Teammachine: surprisingly even one for my budget and size!
I could not let this opportunity pass, so after a short test ride and consideration I quickly made the decision to take it -- and here it is:

{{< figure src="full-bike.avif" alt="The new BMC Teammachine SLR FOUR 2023" >}}

The bike comes with a Teammachine SLR frame - BMC offers *ALR* frames made from aluminium, *SLR* frames made from carbon and *SLR01* made out of higher-grade carbon (that is also lighter)
- this last option is what professional cyclists use, but the frame alone costs 6000+ euros.
Hence, with the *SLR* option you basically get most of the benefits of a carbon frame (lightweight and stiff), at a small sacrifice:
the bike is not *super* light.
As delivered, my bike (with a 61cm frame size) clocks in at 8.6kg.
In return, the bike has incredible stiffness and robustness, which is something I appreciate as a) a tall rider and b) someone who frequently rides on the less-than-perfectly-smooth roads.

Another reason for the slightly higher weight than some of the other bikes in this price range is the fact that this bike comes with proper gearing for the mountains.
It features a 12-speed cassette (!) at the rear and gearing ratio is adjustable all the way from 35x36 (for climbing the steepest mountains) to 48x10 (for fast descents into the valleys).

{{< figure src="groupset-front.avif" alt="SRAM Rival crank and electronic groupset" >}}

The shifting and braking systems are composed entirely from electronic SRAM Rival components.
So far I can only say that they work flawlessly.

For future reference, these are the full specifications of the bike:

|  |  |
|---|---|
Product Name | BMC TEAMMACHINE SLR FOUR - Carbon Roadbike
Model year | 2023
Color | Anthracite / Brushed Alloy
Item Code | BMC577673
Frame | Teammachine SLR Carbon
Groupset | SRAM AXS (electronic)
Chain | SRAM Rival
Cassette | SRAM Rival XG-1250
Gear ratio | 10-36
Chainring ratio | 48-35
Shifter | SRAM Rival eTap AXS HRD
Rear derailleur | SRAM Rival eTap AXS (12-speed, electronic)
Front derailleur | SRAM Rival eTap AXS (electronic)
Crankset | SRAM Rival AXS
Brakes | SRAM Rival eTap AXS HRD (160/160mm disc brakes)
Brake actuation | Hydraulic
Wheels | XRD-522
Wheel size | 28" (622mm)
Tires | Vittoria Rubino (700x25mm)
Seat post | Teammachine SLR Carbon D-Shaped
Saddle | Fizik Antares R7
Handle bar | BMC RAB 02 (Ergo Top Shape, Compact Bend, 70 mm reach, 125 mm drop)
Stem | BMC RSM01
Fork | Teammachine SLR Carbon (43mm offset)
Bottom Bracket | PF86
Weight | 8.6kg (frame size 61)

With the 61cm frame, the bike has a stack of 608mm and a reach of 409mm - perfectly suitable for a rider with 192cm.

{{< figure src="frame.avif" alt="BMC Teammachine SLR FOUR 2023 frame" >}}

After my first ride of close to 400 kilometers, I can already say that I'm super happy with the purchase decision.
Despite being labeled as a "race" bike (as opposed to BMC's Roadmachine series which is intended for endurance), the bike is very comfortable also for long, strenuous rides.
Especially because it gets out of the way and let's the rider focus on pedaling.
I have also noticed that the bike is very agile when cornering - so much so that riding free-handed is quite tricky (though this is not the most common use-case for a race bike).

{{< figure src="frame-color.avif" alt="BMC Teammachine SLR FOUR 2023 - blinking frame coating color" >}}

When I first saw the color of the frame online, I was a bit disappointed: a simple, boring black color, whereas the model from the previous year has an awesome and classic-looking deep blue color.
In reality and under natural light, I have to say that the frame color looks very nice: the color coating has little glitter particles, which make the black color look a whole lot more interesting.
Though to be completely honest, if I could, I would still choose the blue tone of last years model ;-)

Overall, the bikes strikes a very good balance between aerodynamics (the bike is quite aero, though not as much as the "Timemachine" series), robustness (for example, looking at the massive piece of carbon around the bottom bracket, I have no doubt that this bike will last for years to come) and ride comfort (after adjusting the saddle position, I immediately felt like home on the bike despite this being my first time on a BMC bike).

Onto more happy riding!
