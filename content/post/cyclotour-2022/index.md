+++
title = "Cyclotour 2022: Tour around Lake Geneva"
description = "In this post I want to share some impressions from the 'Cyclotour du Léman 2022' - the annual cycling event around Lake Geneva."
categories = "Outdoor"
tags = ["cycling"]
date = "2022-10-29"
+++

On May 22th, the [Cyclotour du Léman 2022](https://cyclotour.ch/news/thank-you-all-for-9481) was carried out.
This is an annual, traditional cycling event around *Lac Léman* (Lake Geneva) happening in the Swiss cantons of *Geneva* and *Vaud* as well as the French department *Haute-Savoie*.

This year, three routes were available: Lausanne to Evian (64 km), Evian to Lausanne (112km) and Lausanne to Lausanne (full lap around the lake, 176km).
The starting time for the full loop is 6:30 am, thus my day started quite early by taking the first train of the day from my hometown Geneva at 5:10 am.
Soon several other riders would join me on the train towards Lausanne.

Upon arriving in Lausanne, I quickly picked up my labels and mounted them to my bike and helmet - the labels are used for time tracking along the route.

{{< figure video="https://s3.cubieserver.de/jack/blog/cycletour-2022/01-departure-lausanne.webm" caption="Departure from Lausanne" width="80%" muted="true" type="video/webm" width="80%" >}}

The ride itself was an absolut blast and one of the fastest rides I ever did - mainly thanks to the fact that there was always the possibility to ride in a suitable group due the large number of riders - this year there were around 3500.
In fact, the first 60 kilometers were practically a continuous Peloton.
For someone like me who does a lot of solo-riding, this is a very welcome and fun change.

{{< figure video="https://s3.cubieserver.de/jack/blog/cycletour-2022/03-neuville.webm" caption="Passing by Swiss spectators in Neuville" width="80%" muted="true" type="video/webm" width="80%" >}}

In addition, I got to enjoy beautiful views over Lac Leman and nearby mountains in the morning hours.
Since it's such a big and popular event, people are standing all along the route and cheer to the passing cyclists, which is great for the motivation.

The terrain is rather flat and the entire route has only 700 meters of cumulative altitude.

Refreshment and nutrition stops were provided at Evian (66km), Messery (97km) and Nyon (137km).
The stops tended to be a bit crowded, but that may also depend on where your pace lies compared to the rest of the riders.

{{< figure video="https://s3.cubieserver.de/jack/blog/cycletour-2022/05-french-side.webm" caption="Cycling along the French side of 'Lac Léman'" width="80%" muted="true" type="video/webm" width="80%" >}}

The route is not closed for traffic - so attention needs to be paid to other vehicles and pedestrian - but at most high-traffic crossings and sections staff are directing the traffic manually (thereby replacing the rules of traffic lights and roundabouts).

{{< figure video="https://s3.cubieserver.de/jack/blog/cycletour-2022/06-geneva.webm" caption="Passing over the 'Pont du Mont Blanc' in Geneva" width="80%" muted="true" type="video/webm" width="80%" >}}

Overall, the event was very well organized and it was a fantastic experience.
If you are interested, the registrations for the [20th edition of the Cyclotour du Léman in 2023](https://cyclotour.ch/) are already open!

{{< figure video="https://s3.cubieserver.de/jack/blog/cycletour-2022/08-arrival-lausanne.webm" caption="Arrival in Lausanne after 176km and 5.5 hours" width="80%" muted="true" type="video/webm" width="80%" >}}

{{< map gpxfile="cyclotour.gpx" >}}
