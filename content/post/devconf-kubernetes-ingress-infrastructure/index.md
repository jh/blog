+++
title = "Scalable and multi-tenant Kubernetes Ingress Infrastructure - DevConf.CZ 2024"
description = "Video recordings and slides for my presentation at DevConf.CZ 2024 in Brno, Czech Republic."
categories = "Software"
tags = ["presentation", "devconf", "kubernetes"]
date = "2024-07-06"
+++

As the [birthplace of the world wide web](https://worldwideweb.cern.ch/), CERN has a long history of running web servers and web sites.
More than 30 years later, we have built a scalable and multi-tenant platform based on the [OKD](https://www.okd.io/) Kubernetes distribution that allows us to handle an extremely diverse set of use cases.

During [my presentation](https://pretalx.com/devconf-cz-2024/talk/PXU8PP/) at [DevConf.CZ](https://www.devconf.info/cz/) I discussed these different use cases and gave a technical overview of the different components we use to implement this infrastructure.
Some of them are well-known open source tools, such as [cert-manager](https://cert-manager.io/), [OpenPolicyAgent](https://www.openpolicyagent.org/) and [external-dns](https://kubernetes-sigs.github.io/external-dns/).
Others are custom built operators and controllers that allow our platform to integrate with CERN's on-premises computing environment.

The PDF slides are available on the [CERN Document Server](https://cds.cern.ch/record/2900715/files/kubernetes-ingress-infrastructure-devconf-cz.pdf) and the video recording can be found on [YouTube](https://www.youtube.com/watch?v=7h4kV5XKunY):

<center>
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/7h4kV5XKunY" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
</center>

DevConf.CZ was a great opportunity to meet up with folks I had already known before (from other conferences or from engagement in the upstream open source communities) as well as connect with new faces.
Despite the fact (or maybe due to the fact?) that DevConf.CZ is primarily sponsored by Red Hat and is attended by many Red Hatters, the conference has a true "community spirit" and does not feel commercial at all.

The venue, the [Brno University of Technology](https://www.vut.cz/en/), certainly also contributes to the relaxed and friendly atmosphere.

{{< figure src="conference-venue.jpg" caption="Inner court yard of the the old monastery that now makes up part of the university campus" >}}

Finally, the conference is also a good excuse to travel to Brno since the city is quite picturesque and offers great food - certainly worth a weekend visit!

{{< figure src="theatre.jpg" >}}

{{< figure src="horse-tram.jpg" >}}
