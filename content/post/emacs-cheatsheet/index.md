+++
title = "Emacs Cheatsheet"
Description = "A comprehensive cheatsheet for Emacs based on 'Mastering Emacs' from Mickey Petersen"
Tags = ["emacs"]
categories = "Software"
date = "2021-01-04"
+++

A couple of weeks ago I have acquired the latest edition of [Mickey Petersen's "Mastering Emacs" book](https://www.masteringemacs.org/).
For anyone looking to learn the fundamentals of *"the way of Emacs"*, I can highly recommend it.

While I have used [Emacs](https://www.gnu.org/software/emacs/) for several years by now, I always felt I was stuck with pretty basic commands and not utilizing the its full potential.
After having read and studied this book, I am still not an Emacs wizard, but I am definitely on the right path.

If you are looking for a guide how to configure Emacs for programming in language X or other specific environments, this book won't help you much.
In general, there are very few suggestions outside of Emacs's included packages (which in my opinion is a good thing, as vanilla Emacs is already complex enough to really get started).

This cheatsheet does not aspire to cover every single one of Emacs' keybindings or commands, but rather this is a subset of keybindings I want to learn and use more often.
As such, it does not include basic keys such as `C-f`, `C-b` etc.
It also assumes familiarity with Emacs lingo.
Therefore, it is nowhere near a replacement for reading "Mastering Emacs" yourself.

All the keybindings shown in this cheatsheet are Emacs defaults.

<style>
    /* set column width for tables (on this page) */
    table tr > *:nth-child(1) {
        width: 25%;
    }
    table tr > *:nth-child(2) {
        width: 49%;
    }
    table tr > *:nth-child(3) {
        width: 25%;
    }
</style>

### Prefix commands

* Universal negative argument: `C--` or `M--`
* Universal numeric argument: `C-N` or `M-N` (with N being a number)

###  Movement

Key Sequence | Action | Notes
---------|----------------|----
`M-a`,  `M-e`            | `backward-sentence` / `forward-sentence`
`C-M-f`                  | Move forward by sexp (`forward-sexp`) | "s-expression"
`C-M-b`                  | `backward-sexp`
`C-M-d`                  | Move down in list of sexp
`C-M-u`                  | Move up in list of sexp
`C-M-k`                  | Kill sexp at point
`C-M-n`                  | Move to next item in list of sexp
`C-M-p`                  | Move to previous item in list of sexp
`M-{`, `M-}`             | Move to beginning / end of paragraph | I'm considering to rebind these to `M-[`, `]`
`C-M-a`, `C-M-e`         | Move to beginning / end of defun
`C-M-v`                  | Scroll up the other window
`C-M-S-v`, `C-M-- C-M-v` | Scroll down the other window
`M-r`                    | Reposition cursor to middle, top, bottom of screen | Cycles
`C-M-l`                  | Recenters the point to middle, top, bottom of buffer | Cycles
`M-g g`                  | Go to line |
`M-g c`                  | Go to character (absolute in buffer)
`M-g TAB`                | Go to column | Refers to the character in current line

### Marking

The direction of all these commands can be inverted with the universal negative argument and a universal numeric argument will mark multiple items.

Key Sequence | Action | Notes
---------|----------------|----
`C-x h`     | Mark the whole buffer
`M-h`       | Mark next paragraph
`C-M-h`     | Mark next defun
`C-M-Space` | Mark next sexp
`M-@`       | Mark next word
`C-u Space` | Jump to last mark
`C-x C-x`   | Exchange point and mark and reactive last region

### Editing

Remember universal numeric and negative arguments!

Key Sequence                                          | Action | Notes
---------                                             |----------------|----
`M-d`, `C-Backspace`                                  | Kill word
`C-k`                                                 | Kill rest of line
`M-k`                                                 | Kill sentence
`C-M-k`                                               | Kill sexp
`C-S-Backspace`                                       | Kill current line | I might rebind this to `C-S-k` (to be more consistent with the rest)
`C-M-t`                                               | `transpose-sexps`
`C-x C-t`                                             | `transpose-lines`
                                                      | `transpose-paragrahps`, `transpose-sentences`
`M-c`                                                 | Capitalize next word | Only first character
`M-u`                                                 | Uppercase next word | All characters
`M-l`                                                 | Lowercase next word | All characters
`M-=`                                                 | Count words, lines and characters (`count-words`)
`C-o`                                                 | Insert new line (without moving point)
`C-x C-o`                                             | Insert all blank lines after point
`M-^`                                                 | Join previous with current line
`M-/`                                                 | Expand word (with DAbbrev or Hippie)
`C-x TAB`                                             | `ident-rigidly` | Negative and numeric arguments!
`M-z`                                                 | `zap-to-char` | Negative argument!
`C-.`                                                 | Auto correct word at point (Cycles through options)
`M-x ispell-buffer`, `ispell-region`                  | Run spell check for buffer / region
`M-x read-only-mode`                                  | Toggles read only mode for buffer
`M-x highlight-phrase`, `-regexp`, `-symbol-at-point` | Highlights a pattern

### Bookmarks

Bookmarks also work across various special modes such as TRAMP.

Key Sequence | Action | Notes
---------    |----------------|----
`C-x r m`    | Set new bookmark
`C-x r l`    | List bookmarks
`C-x r b`    | Jump to bookmark

### Isearch

Key Sequence | Action | Notes
---------    |----------------|----
`M-n`, `M-p` | Move to next / previous item in search history
`C-M-i`      | Auto-complete string from search history
`M-s w`      | Toggle word mode | Will ignore punctuation and delimiters while searching
`M-s Space`  | Toggle lax whitespace matching

### Occur

Occur mode creates a new buffer from lines in the current buffer matching a given pattern (like grep).
This new buffer can then be modified and the changed lines can be written back to the original buffer.

Key Sequence | Action | Notes
---------    |----------------|----
`M-s o`      | `occur-mode`
`M-n`, `M-p` | Go to next / previous occurrence
`<`, `>`     | Go to beginning / end of occur buffer
`g`          | Refresh search results
`e`          | Enable edit mode
`C-c C-c`    | Exit occur mode and apply changes to original buffer
`q`          | Quit occur mode (discards buffer)

### EWW

Key Sequence       | Action | Notes
---------          |----------------|----
`M-x eww`          | Open EWW
`C-u M-x eww`      | Open new EWW buffer
`TAB`, `S-TAB`     | Go to next / previous hyperlink
`l`, `r`           | Go backward / forward in browsing history
`p`, `n`, `u`, `t` | Semantic navigation (if supported by web page)
`R`                | Switch to reader mode
`&`                | Open current page with browse-url
`C-u RET`          | Open link at point in external browser
`M-RET`            | Open link in new EWW buffer
`w`                | Copy link at point
`M-s M-w`          | Search for point / region on the internet
`q`                | Quit EWW
`B`                | Show bookmarks
`b`                | Add bookmark

### Dired

Key Sequence | Action | Notes
---------|----------------|----
`^`           | Goes up one directory (`..`)
`q`           | Quit this dired buffer
`m`, `u`, `t` | Mark active / unactive / toggle
`U`           | Unmark everything
`d`           | Flag for deletion
**Region marking** |
`* m`, `* u`  | Mark / unmark region
`* %`         | Mark files by regexp
`* .`         | Mark files by extension
`* c`         | Change mark
`* t`         | Toggle (invert) marks
**Operations** |
`C`           | Copy files
`R`           | Rename or move files
`O`, `G`, `M` | Change owner / group / permissions
`D`           | Deletes marked files | files with `*`
`x`           | Deletes flagged files | files with `D`
`F`           | Visit files | Opens a buffer for each marked file
`c`           | Compress marked files into archive
`g`           | Refresh buffer
`+`           | Create subdirectory
`s`           | Toggles sorting by name / date
`<`, `>`      | Jump to previous / next directory
`j`           | Jump to file
`!`           | Run shell command (synchronous) | Use argument `*` to have all files in one command, `?` for one command per file
`i`           | Insert subdirectory | Shows another directory in the current dired buffer

### Shell commands

Key Sequence | Action | Notes
---------    |----------------|----
`M-!`        | Run shell command (`shell-command`)
`C-u M-!`    | Run shell command and insert output into buffer
M-\|        | Pipe region into shell command (`shell-on-region`)

### Misc.

Key Sequence | Action | Notes
---------    |----------------|----
`M-x lgrep`  | Grep files specified by glob
`M-x rgrep`  | Recursively greps in specified files

### Helping yourself

As Mickey argues in his book, probably the most important keybindings for Emacs.

Key Sequence                            | Action | Notes
---------                               |----------------|----
`M-x info-apropros`, `apropros-command` | Help for specific topics and commands
`(prefix) C-h`                          | List all keybindings which start with "prefix"
`C-h k`                                 | Describe what a key does
`C-h f`                                 | Describe what a command does
`C-h m`                                 | Describe current mode(s)

-----

Happy hacking!
