+++
title = "Helsinki Sunday Bike Trip"
description = "A beautiful cycling tour passing by Helsinki's tourist attractions and peaceful suburbs, suitable for any fitness level. GPX track available as download."
categories = "Outdoor"
tags = ["cycling", "helsinki", "finland"]
date = "2021-06-20"
image = "roihuvuori.jpg"
+++

This cycling tour leads all around Helsinki (and the neighbouring areas) and is perfect for a relaxed Sunday bike ride.
The terrain is mostly asphalt and compacted gravel, therefore accessible to any type of bicycle.
What I like especially about this route is that you're always close to the sea - so make sure you pick a day that's not too windy!

**Total length**: 60 km

**Total elevation**: 400 m

{{< map gpxfile="sunday-helsinki-trip-edited.gpx" >}}

{{< figure src="eira-sea-view.jpg" caption="View from Eira district towards the sea" height="250px" >}}
{{< figure src="orthodox-church.jpg" caption="Uspenski (Orthodox) Cathedral" height="300px" >}}
{{< figure src="ferris-wheel.jpg" caption="Ferris wheel at the Kauppatori" height="300px" >}}
{{< figure src="kaskisaari.jpg" caption="View towards Kaskisaari Island" height="300px" >}}
{{< figure src="roihuvuori.jpg" caption="Roihuvuori Cherry Park" height="300px" >}}

Enjoy the ride!
