+++
title = "Cloud & Containers - Everything you need to know"
description = "Video recordings and materials for the lecture and workshop I gave at the 14th Inverted CERN School of Computing on the topic of cloud computing."
categories = "Software"
tags = ["cloud", "container", "kubernetes", "presentation", "workshop"]
date = "2023-11-16"
shareimage = "presentation-cover-slide.png"
+++

At the beginning of this year I gave a lecture and workshop at the [Inverted CERN School of Computing 2023](https://indico.cern.ch/event/1208723/).
The [CSC](https://csc.web.cern.ch/) programme covers various aspects of scientific computing for high-energy physics and other data-intensive sciences.

{{< figure src="https://indico.cern.ch/event/1208723/attachments/2579235/4448119/iCSC2023-poster.png" alt="Poster of iCSC 2023" height="500px" >}}

Naturally, my contribution was on the topic of cloud computing.
[During the lecture](https://indico.cern.ch/event/1208723/contributions/5229942/) I covered the history and rise of cloud computing as well as some theoretical concepts.
In the [hands-on workshop](https://indico.cern.ch/event/1208723/contributions/5229946/) participants were able to apply cloud native development practices and become familiar with Kubernetes.

---

*These days, the "cloud" is the default environment for deploying new applications.
Frequently cited benefits are lower cost, greater elasticity and less maintenance overhead.
However, for many people "using the cloud" means following obscure deployment steps that might seem like black magic.*

*This course aims to make newcomers familiar with cloud-native technology (building container images, deploying applications on Kubernetes etc.) as well as explain the fundamental concepts of the tech (microservices, separation of concerns and least privileges, fault tolerance).
In particular, the following topics of application development will be covered:*

* **building**: *writing applications in a cloud-native way (e.g. to work in an immutable environment) and creating container images according to best-practices;*
* **deploying**: *using infrastructure-as-code to describe the application deployment (e.g. Helm charts) and using advanced features such as rolling updates and auto-scaling;*
* **monitoring**: *after multiple containers have been deployed, it is important to keep track of their status and the interaction between the services.*

---

The video recording and slides of the lecture are available:

<iframe width="100%" height="480" frameborder="0" src="https://cds.cern.ch/video/2851915?showTitle=true" allowfullscreen="true"></iframe>

([direct link to video](https://weblecture-player.web.cern.ch/?year=2023&id=1208723c20))

<iframe src="https://indico.cern.ch/event/1208723/contributions/5229942/attachments/2600827/4497993/Cloud%20&%20Containers%20-%20Everything%20you%20need%20to%20know%20-%20Jack%20Henschel.pdf" width="100%" height="500px"></iframe>

([direct link to slides PDF](https://indico.cern.ch/event/1208723/contributions/5229942/attachments/2600827/4497993/Cloud%20&%20Containers%20-%20Everything%20you%20need%20to%20know%20-%20Jack%20Henschel.pdf))

The workshop materials are also available.
While they contain some references to CERN infrastructure, these are just details that can easily be adjusted to other cloud environments.

<iframe src="https://indico.cern.ch/event/1208723/contributions/5229946/attachments/2600822/4498000/Exercise_%20Cloud%20&%20Containers%20-%20Jack%20Henschel.pdf" width="100%" height="500px"></iframe>

([direct link to workshop PDF](https://indico.cern.ch/event/1208723/contributions/5229946/attachments/2600822/4498000/Exercise_%20Cloud%20&%20Containers%20-%20Jack%20Henschel.pdf))


Please feel free to reach out if you'd like to see this content be presented at a different venue (conference, community meeting, company gathering).
