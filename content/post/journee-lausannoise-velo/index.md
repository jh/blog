+++
title = "Journée Lausannoise du Vélo - Lausanne Bicycle Tour 2022"
description = "In this post I want to share my experience of participating in the 2022 edition of the 'Journée Lausannoise du Vélo' - a cycling event in the Swiss canton Vaud."
categories = "Outdoor"
tags = ["lausanne", "switzerland", "cycling"]
date = "2022-06-01"
image = "arrival.jpg"
+++

On Sunday, May 15th 2022, the annual [Journée Lausannoise du Vélo](https://journeeduvelo.ch/) (JVL) took place.
The ride starts at 7:30 am in a small village just outside the [city of Lausanne](https://en.wikipedia.org/wiki/Lausanne), therefore I had to get up early to take the train from Geneva to Lausanne.

{{< figure src="./train-geneva-lausanne.jpg" caption="Watching the sunrise over Lac Léman from the train" width="80%" >}}

The JVL is organized in two disciplines (mountain bikes and road bikes) as well various distances (ranging from 10 to 150 kilometers).
I chose the 150km road bike route which leads from Lausanne northwards to Lake Neuchâtel and back (see the GPX track at the bottom of this post).

For the long category (150km) departures are possible between 7:30 and 9:00 am.
Unless you are riding with a group of friends, I highly recommend starting at 7:30, since most other riders start at that time, too.
And riding in a big bunch of people is not only a ton of fun, but will also give you the opportunity to spontaneously find people with a similar pace to yours, which you can then tag along with for the rest of the tour.

{{< figure video="https://s3.cubieserver.de/jack/blog/journee-lausannoise-velo/jvl-1-departure.mp4" caption="Catching up at the start of the ride" width="80%" muted="true" type="video/mp4" width="80%" >}}

The route (see the bottom of this post) is really scenic and overall well designed.
It's mostly quiet country and side roads, passing by beautiful Swiss villages, farms and meadows.
Since the event is not that big, the roads are not blocked exclusively for cyclist.
The route is well marked with flags and signs, you can also find your way without a GPS.

Especially at the beginning there are many fast and furious downhill sections, which provide a fun start of the ride.

{{< figure video="https://s3.cubieserver.de/jack/blog/journee-lausannoise-velo/jvl-2.mp4" caption="Enjoying one of the fast downhill sections" muted="true" type="video/mp4" width="80%" >}}
{{< figure video="https://s3.cubieserver.de/jack/blog/journee-lausannoise-velo/jvl-3.mp4" caption="Admiring the scenic landscape and stunning villages" muted="true" type="video/mp4" width="80%" >}}

Along the way are refreshments stops which serve bananas, bread, chocolate, cheese, biscuits and of course water and coffee.
This year the stops were at 75km, 105km and 140km.

{{< figure video="https://s3.cubieserver.de/jack/blog/journee-lausannoise-velo/jvl-4-first-stop.mp4" caption="First refreshment stop after 75km" muted="true" type="video/mp4" width="80%" >}}

You might be wondering why there is an additional stop so short before the finish line.
If you look at the height profile of the route, you will quickly understand: the last 45km are the most brutal of the entire ride.
High rolling hills up and down.
In addition, at this point the sun is already quite high in the sky and burns down without mercy - which is especially harsh when you are stuck on an uphill asphalt road with 10km/h and don't have any wind cooling.
Not to mention the fact that by this point most people are pretty tired, too.

{{< figure src="altitude-profile.png" caption="Altitude profile of the route - 1700m in total" >}}

But the challenge is what we are here for, right?
Just stay well hydrated throughout the ride and you will manage to finish it!

Once arrived at the finishing line, you can get some more water and apples, the rest of the food and drinks can be purchased at various booths.
This makes sense because for a starting fee of 25CHF it cannot be expected that the food at the end is included.

{{< figure video="https://s3.cubieserver.de/jack/blog/journee-lausannoise-velo/jvl-6-arrival.mp4" muted="true" type="video/mp4" width="80%" >}}

{{< figure src="arrival.jpg" caption="The bicycle parking is surrounded by booths offering food, beverages and massages" width="80%" >}}

However, what I did instead is head back to Lausanne, specifically the Ouchy Waterfront to take a refreshing bath in the lake, pick up some food and give my legs some rest at the shore front.
On Sundays there is usually something going on there: artists are playing music, merchants are selling regional and foreign food, people are having a good time.

{{< figure src="lac-leman.jpg" caption="Cooling down the legs at the shore of Lac Léman" width="80%" >}}

Overall, I'm very happy with the event and would recommend any cycling enthusiast in the area to give it a try.
It's an excellent opportunity to practice for the Cyclotour du Leman (Tour du Lac), which took place just one week later this year.

**Website**: [Journée Lausannoise du Vélo](https://journeeduvelo.ch/)

**Total length**: 150 km

**Total elevation**: 1733 m

<!-- converted FIT to GPX with: -->
<!-- gpsbabel -t -i garmin_fit -f Journée\ Lausanne\ Velo.fit -o gpx -F jvl.gpx -->

{{< map gpxfile="journee-lausannoise-velo-2022.gpx" >}}

Happy cycling!
