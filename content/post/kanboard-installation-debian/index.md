+++
description = "Full Kanboard Installation with MySQL and LDAP on Debian Jessie"
tags = ["kanboard", "kanban", "debian", "mysql", "ldap"]
date = "2016-10-23T11:09:41+02:00"
updated = "2016-12-17"
title = "Kanboard Installation on Debian Jessie"
categories = "Software"
+++
In this guide I'm describing how to set up Kanboard on a Debian Jessie system with Nginx, MySQL and OpenLDAP.

## What is Kanboard?
[Kanboard](https://kanboard.net/) is a Project Management Software utilizing the [Kanban methodology](https://kanboard.net/documentation/what-is-kanban), it visualizes your workflow and limits your work in progress.

For a complete documentation of how to use Kanboard and utilize Kanban, please visit https://kanboard.net/documentation .

## Requirements
Requirements for Kanboard itself can be found here: https://kanboard.net/documentation/requirements

This guide requires a working web stack consisting of a web server (in my case [Nginx](http://nginx.org/)), a PHP processor ([PHP-FPM](https://php-fpm.org/) here), a MySQL server ([Oracle MySQL](https://www.oracle.com/mysql/index.html) or [MariaDB](https://mariadb.org/)). Optionally, you can also use an LDAP server for authentication like I do ([OpenLDAP](http://www.openldap.org/)). How to set up these won't be explained here, there are enough guides out there on the internet.

## Download
Download the current stable release from [kanboard.net/downloads](https://kanboard.net/downloads) and verify the download.

``` shell
$ wget 'https://kanboard.net/kanboard-latest.zip'
$ wget 'https://kanboard.net/kanboard-latest.zip.asc'
$ gpg --keyserver 'hkp://keys.gnupg.net' --recv-keys 'DCF1 D3CB C1E4 3342 116F  760E 112C 718C 8942 26ED'
$ gpg --verify kanboard-lastest.zip.asc kanboard-latest.zip
```

After successful verification, unpack the folder inside the zip file into your web-root directory.

``` shell
$ unzip kanboard-latest.zip -d /var/www
```

## Database setup
Next, we create the MySQL database and user for Kanboard, so log into your mysql shell.

*Tip:* a good way to generate passwords for things like databases (there is no need to remember the password and you don't need to type it in all the time) is using a tool like `pwgen`.

``` mysql
$ mysql # you might need to add: -uroot -pYOUR_PASSWORD
mysql> CREATE DATABASE kanboard;
# when you're using a remote MySQL instance, change 'localhost' to '%'
mysql> CREATE USER 'kanboard'@'localhost' IDENTIFIED BY 'new_password'
mysql> GRANT ALL PRIVILEGES ON kanboard.* TO 'kanboard'@'localhost';
mysql> FLUSH PRIVILEGES; QUIT;
```

## Configuration
Navigate to the Kanboard installation directory (in my case `/var/www/kanboard`) and make the data directory writable by the web server:

``` shell
chown -R www-data:www-data 'data'
chmod u+rw -R 'data'
```

Move the default configuration file ( `mv config.default.php config.php` ) and edit it with your favorite editor.

### MySQL

``` php
// Database driver: sqlite, mysql or postgres (sqlite by default)
define('DB_DRIVER', 'mysql');

// Mysql/Postgres username
define('DB_USERNAME', 'kanboard');

// Mysql/Postgres password
define('DB_PASSWORD', 'new_password');

// Mysql/Postgres hostname
define('DB_HOSTNAME', 'localhost');

// Mysql/Postgres database name
define('DB_NAME', 'kanboard');
```

[Official Documentation](https://kanboard.net/documentation/mysql-configuration) for MySQL configuration

### LDAP


``` php
// Enable LDAP authentication (false by default)
define('LDAP_AUTH', true);

// LDAP server hostname
define('LDAP_SERVER', 'localhost');

// LDAP server port (389 by default, 636 for ssl)
define('LDAP_PORT', 389);

// By default Kanboard lowercase the ldap username to avoid duplicate users (the database is case sensitive)
// Set to true if you want to preserve the case
define('LDAP_USERNAME_CASE_SENSITIVE', false);

// LDAP bind type: "anonymous", "user" or "proxy"
define('LDAP_BIND_TYPE', 'anonymous');

// LDAP DN for users
// Example for ActiveDirectory: CN=Users,DC=kanboard,DC=local
// Example for OpenLDAP: ou=People,dc=example,dc=c=com
define('LDAP_USER_BASE_DN', 'ou=People,dc=example,dc=com');

// LDAP pattern to use when searching for a user account
// Example for ActiveDirectory: '(&(objectClass=user)(sAMAccountName=%s))'
// Example for OpenLDAP: 'uid=%s'
define('LDAP_USER_FILTER', 'uid=%s');

// LDAP attribute for username
// Example for ActiveDirectory: 'samaccountname'
// Example for OpenLDAP: 'uid'
define('LDAP_USER_ATTRIBUTE_USERNAME', 'uid');
```

[Official Documentation](https://kanboard.net/documentation/ldap-authentication) for LDAP authentication

If you want to allow only for a specific group of your LDAP users (and have the memberof-overlay configured for OpenLDAP), you can apply the following LDAP filter:

``` php
define('LDAP_USER_FILTER', '(&(uid=%s)(memberof=cn=Kanboard,ou=Groups,dc=example,dc=com))');
```

Kanboard has three different profile roles: Users, Managers and Administrators. You can also give LDAP users these roles:

``` php
// LDAP DN for administrators
// Example: CN=Kanboard-Admins,CN=Users,DC=kanboard,DC=local
define('LDAP_GROUP_ADMIN_DN', 'cn=admin,dc=example,dc=com');

// LDAP DN for managers
// Example: CN=Kanboard Managers,CN=Users,DC=kanboard,DC=local
define('LDAP_GROUP_MANAGER_DN', '');
```

However, the local, built-in administrator is called 'admin', just like my LDAP admin. This means I cannot access my LDAP admin, only the built-in admin, because Kanboard searches internal users first. The built-in admin can also not be removed (or at least disabled!).

**Note:** Log into Kanboard with the default admin (Username: admin, Password: admin) and change the password, even if you have a LDAP admin specified!
To do so, log in and click on the icon in the top right corner. Select 'My Profile' and go to 'Actions'. There you can change your password.

All LDAP parameters: https://kanboard.net/documentation/ldap-parameters

In general, the default configuration is very well commented and has sane default values. Nevertheless, it is worth a read to customize it to your needs.

## Conclusion

Kanboard has a ton of features (as can be seen by means of the extensive configuration options) and works very well with groups of people. However, I just wanted a TODO-List with some features such as deadline and categories.
I'm sure Kanboard holds up very well in professional, structured environment, but for my personal use it was just a bit too much.
