+++
title = "Xiaomi Mi A1 LineageOS with microG"
description = "How to unlock the bootloader, flash a custom recovery (TWRP) and install LineageOS with microG on your Xiaomi Mi A1 Global (tissot)"
tags = ["xiaomi","mi a1","lineageos","microg","bootloader","recovery","twrp","android", "tissot"]
categories = "Software"
date = "2018-08-25T16:12:00+02:00"
+++

After getting my [Xiaomi Mi A1 Global (tissot)](https://www.mi.com/in/mi-a1/) I immediately wanted to install LineageOS 15.1 with microG on the device.
After a few hours of research, I found the necessary steps for a successful installation.

Special thanks go to [heinhuiz](https://forum.xda-developers.com/member.php?s=3629cdf97bc1b93a303d9609b894511f&u=4356248) for the [_complete_ instructions](https://forum.xda-developers.com/showpost.php?s=3629cdf97bc1b93a303d9609b894511f&p=77191541&postcount=3520)
and [abhishek987](https://forum.xda-developers.com/member.php?u=6070905) for the TWRP port!

https://forum.xda-developers.com/mi-a1/development/rom-lineageos-15-1-t3757938

If you have installed a custom ROM before, I recommend [flashing the stock ROM](#reset-to-stock-rom) again before starting this guide.
In that case, you can of course skip the bootloader unlocking.
If you still run the stock ROM, update the system to Android 8.1 Oreo and start the guide with [unlocking the bootloader](#unlock-bootloader).

## How to basic

_Note: this is mixed up in quite a few guides._

* Enter recovery mode: `Volume Up + Power Button`
* Enter fastboot mode: `Volume Down + Power Button` (fastboot android will appear)

## Files

I used the following files:

* Android Oreo 8.1 Stock ROM for Mi A1: [9.6.4.0v8.1.zip](https://drive.google.com/drive/folders/1f7o_UnThNgVAOKZYLjprAf65C3YJhDtq)
* [Magisk](https://forum.xda-developers.com/apps/magisk/official-magisk-v7-universal-systemless-t3473445) v16: [Magisk-v16.0.zip](https://github.com/topjohnwu/Magisk/releases/download/v16.0/Magisk-v16.0.zip)
* [TWRP recovery for Oreo](https://forum.xda-developers.com/mi-a1/development/recovery-twrp-3-1-1-0-touch-recovery-t3688472): [recovery-3.2.1-2-oreo.img](https://androidfilehost.com/?fid=818070582850498337)
* [TWRP Installer for tissot](https://forum.xda-developers.com/mi-a1/development/rom-lineageos-15-1-t3757938): [twrp-installer-tissot--3.2.3-0.zip](https://drive.google.com/a/my.smccd.edu/uc?id=1_xM-24m_vANR0eRJrytgnVDAvpiJfC6q&export=download)
* [LineageOS with microG](https://download.lineage.microg.org): [lineage-15.1-20180821-microG-tissot.zip](https://download.lineage.microg.org/tissot/lineage-15.1-20180821-microG-tissot.zip)

## Unlock Bootloader

Beware: will erase all data!

1. Install all regular updates on the phone (as of 2018-08-24: Android Oreo 8.1)
2. Reboot into fastboot mode
3. Unlock bootloader: `fastboot oem unlock`
4. The phone will erase your data, set a new encryption password and boot Android
5. Setup Android

## Reset to Stock ROM

1. Install all regular updates on the phone (as of 2018-08-24: Android Oreo 8.1)
2. Reboot into fastboot mode
3. Boot the recovery: `fastboot boot recovery-3.2.1-2-oreo.img` (will launch TWRP after a few seconds)
4. Select "Wipe" and do a "Factory Reset" including data partition (in TWRP)
5. Push the Stock ROM onto the device ("9.6.4.0v8.1.zip")
6. Install latest Stock ROM
7. Reboot system (no need to install TWRP app)
8. Setup Android Oreo 8.1

## Install LineageOS

1. Enable USB debugging and push all necessary files onto the device with ADB
2. Reboot into fastboot mode
3. Boot the recovery: `fastboot boot recovery.img` (will launch TWRP)
4. "Factory Reset" without wiping data partition
5. Install LineageOS build ("lineage-15.1-20180821-microG-tissot.zip")
6. **Important:** Go to the "Reboot" options and change slots (if slot a is active switch to slot b and vice versa)
7. Install TWRP permanently: "twrp-installer-tissot—3.2.3-0.zip"
8. Install any other ZIPs (openGAPPS, Magisk, SuperSU, ...)
9. Reboot system and setup LineageOS

By the way, since the Xiaomi Mi A1 has a dual camera (which is not fully supported by the AOSP camera), I recommend [installing Xiaomi's stock camera](https://forum.xda-developers.com/mi-a1/themes/oreo-mia1-stock-rom-camera-app-custom-t3758543) through Magisk to get all the features (especially the zoom).

Good luck!

{{< figure src="settings-info.png" alt="System Information" width="60%" >}}

## Thanks

I want to thank these project and their contributors for making this possible:

* [LineageOS](https://lineageos.org/)
* [microG](https://microg.org/)
* [Magisk](https://github.com/topjohnwu/Magisk)
* [Team Win Recovery Project TWRP](https://twrp.me/)

Also thanks to all other free, open-source app developers - you're awesome!
