+++
title = "netcats: libvirt Remote Daemon over SSH"
description = "Running a remote libvirt daemon over SSH on Debian Stretch: Unable to connect to libvirt - remote host requires version of netcat which supports -U option"
tags = ["libvirt", "debian", "stretch", "ssh", "netcat"]
categories = "Software"
date = "2018-04-19T16:03:00+02:00"
+++

For one of my university courses, I need to run a virtual machine with Kali Linux.

Because it is 2018, my laptop is not really fast enough for a VM, I like cloud technology and I love tinkering with stuff, I wanted to use [libvirt's remote support](https://libvirt.org/remote.html)  to run the virtual machine on one of my cloud servers.

First, I installed libvirt by apt-get installing `libvirt-daemon libvirt-daemon-system qemu-kvm`.

Since I already have an SSH connection to the host set up, I chose the [SSH transport](https://libvirt.org/remote.html#Remote_transports), but got the following error message from [virt-manager](https://github.com/virt-manager/virt-manager):

<center>
![libvirt virt-manager error message](libvirt-error-msg.png)
</center>

```
Unable to connect to libvirt.

The remote host requires a version of netcat/nc which supports the -U option.
```

I logged into my server and checked if netcat was installed, which it was.

But there are two versions of netcat on Debian Stretch available:
[netcat-traditional](https://packages.debian.org/stretch/any/netcat-traditional) and [netcat-openbsd](https://packages.debian.org/stretch/any/netcat-openbsd) - only the latter one [supports the required `-U` flag, which specifies to use UNIX-domain sockets](https://manpages.debian.org/stretch/netcat-openbsd/netcat.1.en.html). ([Here is the man page of netcat-traditional, for reference](https://manpages.debian.org/stretch/netcat-traditional/netcat.1.en.html))

You can check which one you have installed with `dpkg`:
```
$ dpkg -l netcat-*
```

After installing netcat-openbsd and removing netcat-traditional, connecting to the remote libvirt daemon over SSH worked just fine.

In the end, I had to recognize that hardware acceleration is not available, as this server is running on a shared host:
```
# virt-host-validate
QEMU: Checking for hardware virtualization                                 : FAIL (Only emulated CPUs are available, performance will be significantly limited)
QEMU: Checking if device /dev/vhost-net exists                             : PASS
QEMU: Checking if device /dev/net/tun exists                               : PASS
QEMU: Checking for cgroup 'memory' controller support                      : PASS
QEMU: Checking for cgroup 'memory' controller mount-point                  : PASS
QEMU: Checking for cgroup 'cpu' controller support                         : PASS
QEMU: Checking for cgroup 'cpu' controller mount-point                     : PASS
QEMU: Checking for cgroup 'cpuacct' controller support                     : PASS
QEMU: Checking for cgroup 'cpuacct' controller mount-point                 : PASS
QEMU: Checking for cgroup 'cpuset' controller support                      : PASS
QEMU: Checking for cgroup 'cpuset' controller mount-point                  : PASS
QEMU: Checking for cgroup 'devices' controller support                     : PASS
QEMU: Checking for cgroup 'devices' controller mount-point                 : PASS
QEMU: Checking for cgroup 'blkio' controller support                       : PASS
QEMU: Checking for cgroup 'blkio' controller mount-point                   : PASS
QEMU: Checking for device assignment IOMMU support                         : WARN (Unknown if this platform has IOMMU support)
 LXC: Checking for Linux >= 2.6.26                                         : PASS
 LXC: Checking for namespace ipc                                           : PASS
 LXC: Checking for namespace mnt                                           : PASS
 LXC: Checking for namespace pid                                           : PASS
 LXC: Checking for namespace uts                                           : PASS
 LXC: Checking for namespace net                                           : PASS
 LXC: Checking for namespace user                                          : PASS
 LXC: Checking for cgroup 'memory' controller support                      : PASS
 LXC: Checking for cgroup 'memory' controller mount-point                  : PASS
 LXC: Checking for cgroup 'cpu' controller support                         : PASS
 LXC: Checking for cgroup 'cpu' controller mount-point                     : PASS
 LXC: Checking for cgroup 'cpuacct' controller support                     : PASS
 LXC: Checking for cgroup 'cpuacct' controller mount-point                 : PASS
 LXC: Checking for cgroup 'cpuset' controller support                      : PASS
 LXC: Checking for cgroup 'cpuset' controller mount-point                  : PASS
 LXC: Checking for cgroup 'devices' controller support                     : PASS
 LXC: Checking for cgroup 'devices' controller mount-point                 : PASS
 LXC: Checking for cgroup 'blkio' controller support                       : PASS
 LXC: Checking for cgroup 'blkio' controller mount-point                   : PASS
```

Oh well. At least I learned something about the [awesome libvirt Virtualization API](https://libvirt.org/index.html) today.
