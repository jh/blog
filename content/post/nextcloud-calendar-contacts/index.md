+++
title = "Getting the most out of Nextcloud Part 2: Calendar, Tasks & Contacts"
description = "The second installment of this series focuses on groupware plugins: Calendar, Tasks, Contacts. I'll show how they work and most importantly how the data can be synchronized onto many other devices (Android, iOS, macOS, Windows, Thunderbird, Linux)."
categories = "Software"
tags = ["nextcloud"]
date = "2024-02-20"
+++

*I've been self-hosting Nextcloud (formerly OwnCloud) for almost a decade in my homelab.
In this series I want to share the features and workflows that have proved to be most useful to me over the years.
You can find all my posts about Nextcloud [here]({{< ref "/tags/nextcloud" >}}).*

---

In the [first part of this series]({{< ref "../nextcloud-files-pictures/" >}}) I showed the most essential features of Nextcloud: file synchronization and sharing.
In this post I'll focus on another set of functionalities that are built-in (like Files), but are much less widely used: [Calendar](#calendar), [Tasks](#tasks) and [Contacts](#contacts).
I'm grouping them in the same post because under the hood they work quite similar -- as we'll see soon.

### Calendar

The [calendar plugin](https://apps.nextcloud.com/apps/calendar) offers a clean and functional web interface for viewing, creating and editing events.
I rarely use the web interface, but it is super useful to coordinate multiple events or to copy&paste information from another source, such as emails or websites.

{{< figure src="calendar.png" caption="The 'monthly' view of Nextcloud Calendar" width="80%" >}}

{{< figure src="calendar-editing-and-settings.png" caption="Creating and editing events via Nextcloud's Calendar" width="80%" >}}

Most frequently I use mobile apps for accessing, creating and editing calendar events on my phone.

On Android the [DAVx⁵](https://www.davx5.com/) app ([Play Store](https://play.google.com/store/apps/details?id=at.bitfire.davdroid), [F-Droid](https://f-droid.org/packages/at.bitfire.davdroid/)) takes care of synchronizing the calendar between the phone and the Nextcloud instance and has been very reliable for many years.
It uses the standardized [CalDav protocol](https://en.wikipedia.org/wiki/CalDAV) for synchronizing events.
If you already have the *Nextcloud Files* app configured on your Android phone, you can use it to set up the DAVx⁵ app:
in the Nextcloud app click on `Settings` > `Sync calendars & contacts`.
A login flow will be initiated where you have to grant access for DAVx⁵.
Then DAVx⁵ will open and ask you to create an account.
Set the account name to one of your choosing, and choose the option `Contact Group Method` to `Groups are per-contact categories`.

{{< figure video="nextcloud-davx5-setup.mp4" alt="How to configure Nextcloud Calendar/Contacts/Tasks with DAVx⁵ and Fossify Calendar on Android" height="540px" >}}

<!-- https://f-droid.org/packages/org.fossify.calendar/ -->

The iOS setup is even simpler because you don't even need to install any app.
A CalDav account can be added directly in the `Settings` app by selecting `Calendar` > `Accounts` > `Add Account` > `Type: Other` > `CalDAV`.
Then type in the domain of your Nextcloud (e.g. `nextcloud.example.com`) and the user name and password (don't forget to use an [app password]({{< ref "../nextcloud-files-pictures#file-synchronization-clients" >}}) for this!).
After confirming with `Select`, the calendar events will show up in the Calendar application.

> Note: it is **very** important that the ["well-known" redirections](https://docs.nextcloud.com/server/latest/admin_manual/issues/general_troubleshooting.html#service-discovery) are set up properly, otherwise iOS will not be able to connect to Nextcloud.

Visit the [Nextcloud documentation](https://docs.nextcloud.com/server/stable/user_manual/en/groupware/calendar.html) to find out about all the features of the Nextcloud Calendar plugin.
It is also possible to synchronize the calendar events to [macOS](https://docs.nextcloud.com/server/latest/user_manual/en/groupware/sync_osx.html), [Thunderbird](https://docs.nextcloud.com/server/latest/user_manual/en/groupware/sync_thunderbird.html), [Windows](https://docs.nextcloud.com/server/latest/user_manual/en/groupware/sync_windows10.html), [GNOME](https://docs.nextcloud.com/server/latest/user_manual/en/groupware/sync_gnome.html) and [KDE](https://docs.nextcloud.com/server/latest/user_manual/en/groupware/sync_kde.html) (click the links for setup instructions).

{{< figure video="thunderbird-calendar-tasks.mp4" caption="Setting up Thunderbird to synchronize tasks and events from Nextcloud" width="80%" >}}

<!-- Calendar 4.6.4 -->

#### Appointment scheduling

The Calendar plugin also has the ability to act as a tool scheduling meeting with other people.
The way it works is that you create a new *Appointment* from your calendar, specify some metadata and define the allowed time windows.
You can then distribute a link to the invitee(s), when they open it they can choose one of the available time slots.
After the appointment is confirmed, both the organizer and the invitee will automatically receive a calendar invitation via email.

{{< figure src="create-appointment.png" caption="Nextcloud Calendar: Appointment creation view" width="80%" >}}

{{< figure src="appointment-guest.png" caption="Nextcloud Calendar: Appointment guest view" width="80%" >}}

Much more convenient than sending a dozen emails back and forth!

### Tasks

[Nextcloud Tasks](https://apps.nextcloud.com/apps/tasks) offers a sleek web interface for To-Do lists.
Under the hood it actually uses the same protocol as the Calendar plugin ([CalDav](https://en.wikipedia.org/wiki/CalDAV)), which means that the synchronization clients you've set up in the previous step will also automatically synchronize tasks -- neat!
Tasks allow you to set priorities, start & due dates, completion status and add notes.

> Note for administrators: unlike Calendar and Contacts, the Tasks plugin is not enabled by default, therefore you'll have to install it from the [Nextcloud "App store"](https://apps.nextcloud.com/apps/tasks).

{{< figure src="tasks.png" alt="Nextcloud Tasks web interface" width="80%" >}}

However, depending on your platform, you might need an additional app to actually view them.
On Android I recommend `Tasks.org` ([Play Store](https://play.google.com/store/apps/details?id=org.tasks), [F-Droid](https://f-droid.org/en/packages/org.tasks/)) because it is simple to use and yet packed with features (have a look at [their docs](https://tasks.org/docs/filters)).

{{< figure src="android-tasks.png" caption="Tasks.org Android App (left) and home screen widget (right)" width="80%" >}}

If you'd like a bit more structure to your tasks or you are not a "to-do list person", take a look the [Deck plugin](https://github.com/nextcloud/deck) which offer Kanban-style organization for personal tasks and project organization.

<!-- Tasks 0.15.0 -->

### Contacts

[Nextcloud Contacts](https://apps.nextcloud.com/apps/contacts) is the final piece of the groupware trilogy.
As the name suggests, it allows you to manage contact information and use it as an address book for mail clients etc.
The plugin exposes the [CardDav protocol](https://en.wikipedia.org/wiki/CardDAV), which like in the previous cases can be used by many synchronization clients.

{{< figure src="contacts.png" caption="Nextcloud Contacts web view" width="80%" >}}

On Android DAVx⁵ takes care of synchronizing contacts as well, simply follow the setup instructions outlined in the [Calendar](#calendar) section above.
In case the contacts don't appear on your phone, you may need to enable the additional source in the app you use for viewing contacts.
This can usually be found in the settings of the contacts app under `Accounts`.

On iOS the setup is very similar to the [calendar setup described before](#calendar): open the the `Settings` app, head to `Contacts` > `Accounts` > `Add Account`, choose Type `Other` > `CardDAV` and then type in the connection details of your Nextcloud instance - don't forget to use an [app password]({{< ref "../nextcloud-files-pictures#file-synchronization-clients" >}})!

Of course contacts are not only important on mobile devices, but also for mail clients.
Since version 102 Thunderbird offers a native integration for CardDAV/CalDAV.

{{< figure video="thunderbird-contacts.mp4" caption="Setting up Thunderbird to synchronize an address book from Nextcloud Contacts" width="80%" >}}

Similarly, contacts can also be synchronized with [macOS](https://docs.nextcloud.com/server/23/user_manual/en/groupware/sync_osx.html), [Windows](https://docs.nextcloud.com/server/23/user_manual/en/groupware/sync_osx.html), [KDE](https://docs.nextcloud.com/server/23/user_manual/en/groupware/sync_osx.html) and [GNOME](https://docs.nextcloud.com/server/23/user_manual/en/groupware/sync_gnome.html) (click the respective links for setup instructions).

<!-- Contacts 5.5.1 -->

> P.S. Did you become interested in Nextcloud? A handy migration tool allows you to import your contacts and calendar from Google. Find more details [here](https://nextcloud.com/blog/easy-migration-to-nextcloud-from-insecure-and-privacy-unfriendly-platforms-now-available/).

Happy synchronizing!
