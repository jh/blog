+++
title = "Getting the most out of Nextcloud Part 1: Files & Pictures"
description = "In the first part of this series, I'll dive into the two essential and powerful plugins for Nextcloud: 'Files' for synchronizing and sharing files, 'Memories' for exploring, sorting and viewing pictures and videos."
categories = "Software"
tags = ["nextcloud"]
date = "2024-02-05"
shareimage = "cover-image.png"
+++

*I've been self-hosting Nextcloud (formerly OwnCloud) for almost a decade in my homelab.
In this series I want to share the features and workflows that have proved to be most useful to me over the years.
You can find all my posts about Nextcloud [here]({{< ref "/tags/nextcloud" >}}).*

---

Nextcloud is most commonly known as a personal file sharing and synchronization software, similar to Dropbox or Google Drive.
Certainly for me, this feature has been the metaphorical gateway drug into the Nextcloud ecosystem.
In this post I'll share what the 'Files' plugin can do, how to use the mobile clients and why the 'Memories' plugin is awesome for pictures.

{{< figure src="nextcloud-files.png" caption="The 'Files' viewer of Nextcloud 28" >}}

Nextcloud Files lets you upload files via drag&drop, create folders and share links to individual files or folders (either with other users in the same Nextcloud, with users on another Nextcloud instance or via a public sharing link):

{{< figure src="sharing-menu.png" alt="Nextcloud File Sharing Menu" >}}

In recent releases (version 25+), Nextcloud has also gotten some *AI capabilities*: automatically tagging files (documents/pictures/music) based on content, translations and other recommendations.
I won't go into the details here, but you should definitely take a look at the [AI documentation](https://docs.nextcloud.com/server/latest/admin_manual/ai/index.html).

{{< figure src="file-tags.png" caption="Automatically generated tag categories" >}}

A full walkthrough of the Nextcloud web interface and its features is [available in the documentation](https://docs.nextcloud.com/server/stable/user_manual/en/webinterface.html).

Unlike other platforms, Nextcloud does not lock you in, but provides a well-known and standardized protocol to access files: [WebDAV](https://docs.nextcloud.com/server/latest/user_manual/en/files/access_webdav.html).
This HTTP based protocol allows other applications to connect to Nextcloud, download and upload files and even "mount" some directories locally.
Examples include [KeepassDX](https://www.keepassdx.com/) (an Android app that fetches the KDBX password file from Nextcloud) and [Nextcloud Notes](https://apps.nextcloud.com/apps/nextcloud_notes_android_app) (the Android/iOS app stores notes as simple text files in Nextcloud).
Hopefully this will get picked up by more app developers in the future, so you can use your Nextcloud instance as a backend storage for every app! (instead of relying on proprietary sync mechanisms and platforms)

Check out the [Nextcloud App Store](https://apps.nextcloud.com/categories/files) for more awesome Nextcloud plugins that work with files (such as [Checksum](https://apps.nextcloud.com/apps/checksum), [Metadata viewer](https://apps.nextcloud.com/apps/metadata), [Archive Manager](https://apps.nextcloud.com/apps/files_archive) or various document signing solutions).

### File synchronization clients

The Nextcloud desktop client is available for Linux, Mac OS and Windows.
It allows you to continuously synchronize all of your files, or just a subset.

{{< figure src="desktop-client.png" caption="Nextcloud desktop synchronization client (in dark mode)" >}}

Note that the Nextcloud client allows you to connect to multiple accounts and multiple Nextcloud instances at the same time.
This can be handy if you have private and professional accounts or instances, for example.

The same feature set is also offered by the Nextcloud mobile client, which is available for Android ([Play Store](https://play.google.com/store/apps/details?id=com.nextcloud.client) / [F-Droid](https://f-droid.org/packages/com.nextcloud.client/)) and iOS ([App Store](https://apps.apple.com/us/app/nextcloud/id1125420102)).
In addition, the mobile clients allow you to set up "Auto Upload" of all pictures taken on your phone.
(Personally I'm not using this feature, though.)

I have used the mobile app countless times to quickly access documents, pictures and notes while on the move.
It also serves as an entrypoint and authentication source for other apps: Nextcloud Talk, Nextcloud Notes and DAVx5 - I'll discuss these in future posts.

{{< figure src="android-app.png" caption="Nextcloud Android App (in dark mode)" height="600px" >}}

For setting up the mobile app, I **highly** recommend to use a so called [app password](https://docs.nextcloud.com/server/stable/user_manual/en/session_management.html) for specific devices and apps.
This improves security because the app never gets your real password and you can revoke access at any time from the Nextcloud web interface - instead of having to reset your actual password.
Best of all: many apps support reading this data directly from a QR code!

{{< figure src="app-passwords.png" caption="How to create an 'app password' from the Nextcloud web interface" >}}

### Pictures

As you can see from one of the screenshots above, the majority of files I store in Nextcloud are actually pictures.
Nextcloud has an integrated picture viewer (called [Photos](https://github.com/nextcloud/photos)), but recently a much better alternative has emerged: [Memories](https://apps.nextcloud.com/apps/memories).
It offers a timeline feature (chronological view of all pictures), editing pictures, organizing pictures into albums (independent from the directory structure), EXIF metadata viewing, and a map for viewing the location where pictures were taken -- this is one of my favorite features personally!

Most importantly, the developer has spent lots of time optimizing the preview and transcoding features, meaning that it's **fast** to view photos & videos.
To enable all of the features of Memories and get the most performance, it is important to follow the [setup steps](https://memories.gallery/config/).

{{< figure src="memories-on-this-day.png" caption="Memories 'On This Day' view" >}}

---

{{< figure src="memories-details-tags.png" caption="Memories can view and edit EXIF metadata; tags are generated by the 'Recognize' plugin" >}}

---

{{< figure src="memories-picture-editing.png" caption="The built-in editor is perfect for quick and simple post-processing tasks" >}}

---

{{< figure src="memories-map.png" caption="The map view allows visualizing where you were and what you did" >}}

---

The only minor downside is that all of these awesome features of "Memories" are only available in the Nextcloud web interface, but no native mobile app is available.
However, there are a couple of other "picture viewer" apps: checkout [Les Pas](https://github.com/scubajeff/lespas#readme) and [Yaga](https://vauvenal5.github.io/yaga-docs/) for Android.

Happy memories!
