+++
title = "Namespace-scoped Kubernetes Service Discovery for Prometheus"
description = "How to set up ServiceAccount, Role, RoleBinding and configure Prometheus to only discover scraping targets from the local Kubernetes namespace."
tags = ["prometheus", "k8s"]
categories = "Software"
date = "2021-04-16"
updated = "2021-05-19"
+++

Recently I was helping a colleague setting up Prometheus monitoring for an application.
This application must be installed into the Kubernetes cluster without `cluster-admin` rights.
Therefore ClusterRoleBindings are not an option.
Instead we need to use a regular RoleBinding for attaching the the ServiceAccount to the Role.

*If you are not familiar with the relation between roles, service accounts and role bindings, please refer to [Kubernetes' documentation on RBAC](https://kubernetes.io/docs/reference/access-authn-authz/rbac/).*

It boils down to adding the following YAML snippets to the Prometheus deployment (inspired by the [Prometheus Helm Chart](https://github.com/prometheus-community/helm-charts/blob/e2ef0e5cb8cbb17bcc6385b2d803d05fd7729e86/charts/prometheus/values.yaml#L1288)):

```yaml
---
apiVersion: rbac.authorization.k8s.io/v1
kind: Role
metadata:
  name: prometheus-server-role
  labels:
    app.kubernetes.io/name: prometheus-server
rules:
  - apiGroups: [""]
    resources:
      - endpoints
      - pods
      - services
    verbs:
      - get
      - list
      - watch
---
apiVersion: v1
kind: ServiceAccount
automountServiceAccountToken: true
metadata:
  name: prometheus-server-sa
  labels:
    app.kubernetes.io/name: prometheus-server
---
apiVersion: rbac.authorization.k8s.io/v1
kind: RoleBinding
metadata:
  name: prometheus-server-rb
  labels:
    app.kubernetes.io/name: prometheus-server
subjects:
- kind: ServiceAccount
  name: prometheus-server-sa
  namespace: mynamespace
roleRef:
  kind: Role
  name: prometheus-server-role
  apiGroup: rbac.authorization.k8s.io
```

and adding the [service account](https://v1-20.docs.kubernetes.io/docs/reference/access-authn-authz/service-accounts-admin/) to the [PodTemplate specification](https://v1-20.docs.kubernetes.io/docs/reference/kubernetes-api/workload-resources/pod-v1/):

```yaml
apiVersion: apps/v1
kind: StatefulSet
...
spec:
  ...
  template:
    spec:
      serviceAccountName: prometheus-server-sa
```

*Please note that with a namespace-scoped service account Prometheus won't be able to scrape the commonly used jobs `kubernetes-apiservers`, `kubernetes-nodes` and `kubernetes-nodes-cadvisor` because they are using the `node` role*

So far so good.
Unfortunately, it still didn't work.
We were using the following, simplified Prometheus configuration:

```yaml
global:
  scrape_interval: 15s
  scrape_timeout: 10s
  evaluation_interval: 1m

scrape_configs:
  - job_name: prometheus
    static_configs:
      - targets:
	- localhost:9090

  - job_name: 'kubernetes-service-endpoints'
    kubernetes_sd_configs:
      - role: endpoints
    relabel_configs: ...
```

And Prometheus was giving us these errors:

```
$ kubectl logs deploy/prometheus-server -c prometheus-server | tail
Failed to list *v1.Pod: pods is forbidden: User "system:serviceaccount:mynamespace:prometheus-server" cannot list resource "pods" in API group "" at the cluster scope
```

The last two words of the log message contain the the key: `cluster scope`.
Prometheus is still trying to scrape at the cluster-level, even though the associated service account only allows it to scrape at the namespace level.

Unfortunately, [Prometheus documentation on Kubernetes Service Discovery](https://prometheus.io/docs/prometheus/latest/configuration/configuration/#kubernetes_sd_config) is not very clear about how to configure namespace-only scraping.
[This Github issue comment](https://github.com/prometheus/prometheus/issues/2763#issuecomment-458054220) led me to the correct configuration option:

```yaml
kubernetes_sd_configs:
  - job_name: 'foobar'
    namespaces:
      names:
        - "mynamespace"
```

For each job definition under the `kubernetes_sd_configs` key we need to specify for which namespace(s) it is activate.
If no namespaces are specified, Prometheus will try to scrape all namespaces --- and if it is not allowed to do that, the entire process will fail.

The updated example configuration shown above looks like this:

```diff
  scrape_configs:
    - job_name: prometheus
      static_configs:
        - targets:
          - localhost:9090

    - job_name: 'kubernetes-service-endpoints'
      kubernetes_sd_configs:
        - role: endpoints
+         namespaces:
+          names:
+            - "mynamespace"
      relabel_configs:...
```

After this configuration change Prometheus discovered all the namespace-local services.

If you want to achieve the same effect when using the [Prometheus Helm Chart](https://github.com/prometheus-community/helm-charts) (v13+), you need to specify the following Helm values:

```yaml
server:
  useExistingClusterRoleName: false
  namespaces:
    - "mynamespace"
```

Happy scraping!
