+++
title = "When One Line Took Thousands of Websites Offline"
description = "Video recordings and slides for my presentation at SREcon EMEA 2023 in Dublin, Ireland."
categories = "Software"
tags = ["presentation", "srecon"]
date = "2024-01-25"
image = "covention-centre-dublin.jpg"
+++

At the end of 2022, CERN's web infrastructure suffered a major outage: thousands of internal and external websites became unavailable within minutes.
My colleague [Francisco Borges Aurindo Barros](https://www.linkedin.com/in/francisco-aurbarros) and myself were the ones doing the troubleshooting and cleanup on that day (and the following days).
Almost a year later, we presented the incident at the [USENIX Site Reliability Engineering Conference](https://www.usenix.org/conference/srecon23emea) (SREcon) in Dublin: timeline of the event, impact, root cause analysis, and most importantly the technical and process improvements we have made since then.

The PDF slides are available [here](https://www.usenix.org/system/files/srecon23emea-slides_barros.pdf) and the video recording can be found [on YouTube](https://www.youtube.com/watch?v=pADGTICt_aQ):

<center>
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/pADGTICt_aQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
</center>

I really enjoyed the atmosphere at the conference and talking to the other participants and speakers: compared to other "industry conferences" this one felt much less commercial and lot more focused on content.
The venue (Convention Centre Dublin) was also not *too* big, which is important because that avoids having to walk eternal distances between each presentation.
Finally, exploring the pubs and distilleries of Dublin added to the experience. :-)

{{< figure src="covention-centre-dublin.jpg" caption="View of the Convention Centre Dublin across the River Liffey" >}}
