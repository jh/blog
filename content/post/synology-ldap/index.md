+++
title = "Synology NAS: Samba, NFS and Kerberos with FreeIPA LDAP"
description = "How to set up your Synology NAS DiskStationManager (DSM) for Samba and NFS with Kerberos using an external LDAP server"
tags = ["synology", "nas", "samba", "smb", "kerberos", "nfs", "freeipa", "ldap"]
categories = "Software"
date = "2018-07-06T10:00:00+02:00"
updated = "2018-11-20"
+++

*This work is a collaboration with my colleague [Markus Opolka](https://martialblog.de/) ([@martialblog](https://github.com/martialblog/)).*

Since we migrated our old, hacky LDAP server to a completely new FreeIPA instance, authenticating Samba and NFS users with the new LDAP server (provided by FreeIPA) was no longer possible.

As we don't have that many users, the short-term fix was to locally create the required accounts on the Synology NAS.
This has the disadvantage of splitting the password management, so we wanted to fix it.

Unfortunately, Synology's documentation on this issue is rather sparse.

Here is what we found out through a lot of internet research, searching through log files and digging in the configuration.

Before we begin: we are running Synology DSM 6.1 and FreeIPA 4.4.

## Samba

* The LDAP user accounts need `sambaSamAccount` as `objectClass`.
* `sambaSamAccount` requires the following three attributes: `sambaSID`, `sambaAcctFlags` and `sambaDomainName`
* `sambaSID` is the unique id of the user, it follows a scheme like `S-1-5-21-UID` where the last part is unique for each user (the rest is shared among the domain), more information: https://support.microsoft.com/en-us/help/243330/well-known-security-identifiers-in-windows-operating-systems
* `sambaAcctFlags` is set to "[U&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;]" (12 characters between the brackets!), more information: http://pig.made-it.com/samba-accounts.html
* `sambaDomainName` is simply the domain name of your Samba domain (all caps).

Unfortunately, FreeIPA's web interface does not allow setting 'custom' attributes (like the ones shown above), hence users can no longer be created via the Web-UI (since the attributes are mandatory), but have to be created from the command line:

```
ipa user-add frankdoe --first=Frank --last=Doe --password --addattr=sambaSID=S-1-5-21-122 --addattr=sambaAcctFlags=[U           ]
```

Existing users can be modified with the following LDIF script:

```ldif
dn: uid=johndoe,cn=users,cn=accounts,dc=example,dc=com
changetype: modify
add: objectClass
objectClass: sambaSamAccount
-
add: sambaSID
sambaSID: "S-1-5-21-123"
-
add: sambaAcctFlags
sambaAcctFlags: "[U           ]"
-
add: sambaDomainName
sambaDomainName: "EXAMPLE.COM"
```

and applied:

```
ldapmodify -h localhost -x -D "cn=Directory Manager" -W -f /path/to/samba.ldif
```


**Important step**: grant your LDAP service bind account access to the relevant attributes!

* Go to "IPA Server" and create a new role "File Server"
* Create a new privilege "Samba Authentication"
* Add a new permission "Read Samba Attributes" to this privilege
* Select the various Samba attributes listed
* Add the newly created role to the bind account

![samba permissions](samba-permissions.png)

Since your users probably don't have the `NTPasswordHash` attribute set yet, they will have to reset (i.e. retype) their password in the Web-UI once, then FreeIPA will automatically set the password hash.

## NFS

NFS authentication via LDAP and Kerberos was previously working, however we had trouble with the ID mappings.
If you have local users on the Synology NAS, you can manually map the UID (Control Panel -> File Services -> NFS -> Kerberos Settings -> ID Mapping), but then the users are still using the 'local' password on the NAS.

So let's fix that, too!

Since the NFS LDAP is not included with FreeIPA, get the UMICH schema from http://www.citi.umich.edu/projects/nfsv4/crossrealm/libnfsidmap_config.html (at the bottom of the page) and insert it into `/etc/dirsrv/slapd/schema/99nfs.ldif`.

```ldif
dn: cn=schema
attributeTypes: ( 1.3.6.1.4.1.250.1.61 NAME 'NFSv4Name' DESC 'NFS version 4 Name' EQUALITY caseIgnoreIA5Match SYNTAX 1.3.6.1.4.1.1466.115.121.1.26 )
attributeTypes: ( 1.3.6.1.4.1.250.1.62 NAME 'GSSAuthName' DESC 'RPCSEC GSS authenticated user name' EQUALITY caseIgnoreIA5Match SYNTAX 1.3.6.1.4.1.1466.115.121.1.26 )
objectClasses: ( 1.3.6.1.4.1.250.1.63 NAME 'NFSv4RemotePerson' DESC 'NFS version4 person from remote NFSv4 Domain' SUP top STRUCTURAL MUST ( uidNumber $ gidNumber $ NFSv4Name ) MAY ( cn $ GSSAuthName $ description) )
objectClasses: ( 1.3.6.1.4.1.250.1.64 NAME 'NFSv4RemoteGroup' DESC 'NFS version4 group from remote NFSv4 Domain' SUP top STRUCTURAL MUST ( gidNumber $ NFSv4Name ) MAY ( cn $ memberUid $ description) )
```

* The user accounts need `NFSv4RemotePerson` as `objectClass`
* `GSSAuthName` needs to be set to `username@EXAMPLE.COM`
* `NFSv4Name` needs to be set to `username@example.com`

This can be achieved with this LDIF snippet:

```ldif
dn: uid=johndoe,cn=users,cn=accounts,dc=example,dc=com
changetype: modify
add: objectClass
objectClass: NFSv4RemotePerson
-
add: GSSAuthName
GSSAuthName: johndoe@EXAMPLE.COM
-
add: NFSv4Name
NFSv4Name: johndoe@example.com
```

and again applied with:

```
ldapmodify -h localhost -x -D "cn=Directory Manager" -W -f /path/to/nfs.ldif
```

Again, we need to grant our LDAP service bind access to these 'new' attributes.

* In the FreeIPA UI: Extend the previously created role "File Server"
* Create new privilege "Kerberos Authentication"
* Add new permission "Read NFS Attributes" to this privilege
* Grant `read`, `search` and `compare` for the attributes `GSSAuthName` and `NFSv4Name`
* Note: Since these attributes are not native to FreeIPA, you have to type `gssauthname` into "Effective Attributes", hit "Add", then insert the attribute name again ("Add Custom Attributes" dialog), hit enter and now (finally!) the attribute appears with a check box next to it - repeat for `nfsv4name`

By default, Synology NAS creates the home directory for the user at `/home/@LH-${FQDN}/${some_number}/${user}-${uid}`.
But we don't want to follow their scheme, therefore we disable the auto-creation of home directories on the NAS and manually create the home directory and set the owner to `johndoe@example.com`.

Don't forget to synchronize the LDAP between your LDAP server and your NAS (Control Panel -> LDAP -> LDAP Users -> Update LDAP Data).

One more thing: we strongly discourage using Synology's Web-UI to modify the ownership of directories since it discards the modes of the files.
Rather, login via SSH and set the appropriate owner with `chown`.

## Debugging

If authentication is still not functioning, here are two tips for debugging:

* Use `smbclient` from a Linux machine to connect the server, since its verbosity can be drastically increased (unlike Windows), e.g.: `smbclient -d 10 -U johndoe //file-server-address/Share-Name`.
* Enable the verbose debug logs in Control Panel -> File Services -> SMB -> Advanced Settings -> Collect Debug Logs, log into the NAS via SSH and look at the logs under `/var/log/samba/`.

## References

* https://www.freeipa.org/page/Howto/Integrating_a_Samba_File_Server_With_IPA
* https://aput.net/~jheiss/samba/ldap.shtml
* https://bgstack15.wordpress.com/2017/05/10/samba-share-with-freeipa-auth/
* https://www.redhat.com/archives/freeipa-users/2015-August/msg00137.html
