+++
title = "Investigations into Video Streaming"
description = "Reports about how video streaming and distribution with CDNs works, how (well) adaptive bitrate streaming works, and latency comparisons between various live streaming protocols"
tags = ["video", "streaming"]
categories = "Software"
date = "2020-02-16T20:00:56+03:00"
+++

As part of my studies, [Max Crone](https://maxcrone.org/) and I have been working together in a course called "Applications and Services in Internet".
It focused on different video streaming applications which are widely used on the internet.

## An Analysis of the YouTube CDN

*This report is a research into the video distribution
and caching strategies employed by YouTube. We collect data
from multiple continents for a multitude of videos. Our analyses
find geolocations of cache servers used by YouTube, measure
their performance and interpret the cache host names. Our data
did not support any clear strategy based on time of the week or
region, with the exception of the case for uploading a video, in
which the region of upload had a clear advantage for the first
hour after publishing compared to other regions. We conclude
by formulating our expectation that the distribution and caching
in YouTube’s infrastructure is currently largely governed by
machine learning models and therefore our research could not
find a clearly discernible strategy.*

[Download PDF: An Analysis of the YouTube CDN](report1-analysis-of-youtube-cdn.pdf)

---

## An analysis of adaptive video streaming with DASH

*In this report we investigate the behaviour of Adap-
tive Bitrate Streaming with the DASH protocol under different
network conditions with regard to video quality and fairness
between clients. We found that the adaption to static and
changing network conditions works quite well to always provide
clients with the highest possible video quality while minimizing
buffering delays.*

[Download PDF: An analysis of adaptive video streaming with DASH](report2-analysis-of-dash.pdf)

---

## Live streaming latency of streaming protocols

*In this report we set up a live streaming server using
a combination of stateless and stateful protocols, based on which
we evaluate the performance of these different protocols with
regards to latency. As expected, we find that a stateful protocol
like RTMP has the lowest latency while suffering from scalability
limitations. Stateless protocols, such as HLS and DASH, exhibit
a higher latency but are easier to scale, e.g. by using a CDN
which we also analyzed. Specific fine-tuning of the streaming
parameters for the stateless protocols is found to bring down
their end-to-end-latency.*

[Download PDF: Live streaming latency of streaming protocols](report3-live-streaming-protocols-latency.pdf)

-----

We hope you find these articles helpful.
For details, setup scripts and more, check out the source repository: https://version.aalto.fi/gitlab/henschj1/apps-2019
