+++
title = "Jack Henschel's Publications"
date = "2018-05-05"
description = "Articles, papers and presentations by Jack Henschel"
lastmod = "2021-10-26"
+++

## Dimensioning, Performance and Optimization of Cloud-native Applications (Master's thesis)

Title: | Dimensioning, Performance and Optimization of Cloud-native Applications
 --- | ---
 Abstract: | Cloud computing and software containers have seen major adoption over the last decade. Due to this, several container orchestration platforms were developed, with Kubernetes gaining a majority of the market share. Applications running on Kubernetes are often developed according to the microservice architecture. This means that applications are split into loosely coupled services that are distributed across many servers. The distributed nature of this architecture poses significant challenges for the observability of application performance. We investigate how such a cloud-native application can be monitored and dimensioned to ensure smooth operation. Specifically, we demonstrate this work based on the concrete example of an enterprise-grade application in the telecommunications context. Finally, we explore autoscaling for performance and cost optimization in Kubernetes i.e., automatically adjusting the amount of allocated resources based on the application load. Our results show that the elasticity obtained through autoscaling improves performance and reduces costs compared to static dimensioning. Moreover, we perform a survey of research proposals for novel Kubernetes autoscalers. The evaluation of these autoscalers shows that there is a significant gap between the available research and usage in the industry. We propose a modular autoscaling component for Kubernetes to bridge this gap.
Organization: | [Aalto University](https://www.aalto.fi/en), [EURECOM](https://www.eurecom.fr), [Ericsson Finland](https://www.ericsson.com/en)
Author: | Jack Henschel
License: | [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
Download: | [master_Henschel_Jack_2021.pdf](https://aaltodoc.aalto.fi/bitstream/handle/123456789/109318/master_Henschel_Jack_2021.pdf)

## Intel PT Hooking (Bachelor's thesis)

Title: | Intel PT Hooking
 --- | ---
 Abstract: | In this work, we implement a hooking technique based on Intel Processor Trace hardware unit. The original idea was implemented by CyberArk Labs on Microsoft Windows, we tried to build a functionally equivalent port for Linux-based operating systems. However, it does not fulfill the same use-case, since we had to recourse to using invasive techniques for registering the interrupt handler. Additionally, we present a novel technique for tracing system and library calls with Intel PT. This technique is not yet fully reliable, as individual calls may be lost, but we outline how to overcome this technical impediment.
Organization: | [FAU](https://www.fau.eu), [IT Security Infrastructures Lab](https://www.cs1.tf.fau.de)
Author: | Jack Henschel
License: | [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
Download: | [Henschel_Intel-PT-Hooking_2018.pdf](./Henschel_Intel-PT-Hooking_2018.pdf)

## Linux Tracing Frameworks

Title: | Tracing Frameworks
 --- | ---
Abstract: | Tracing Frameworks provide a direct interface to inspect, test, debug and measure running applications (so called “online code”). This can be valuable while developing a program (e.g. for performance analysis or error checking), but also when troubleshooting issues after deployment in the field. Some frameworks don’t require modification of application source code at all, others depend on entry points (“markers”) or loading additional libraries. This paper demonstrates the usage of SystemTap and evaluates two other tracing frameworks (Frida and LTTng) at the end.
Organization: | Nokia Solutions and Networks GmbH & Co. KG
Date: | March 2017
Author: | Jack Henschel
License: | [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
Download: | [Henschel_Linux-Tracing_2017.pdf](./Henschel_Linux-Tracing_2017.pdf)

## Intel Processor Trace (intel_pt)

Title: | Intel Processor Tracing
--- | ---
Abstract: | Intel Processor Trace (PT) is a new feature of Intel processors which provides machine instruction-level tracing. This can aid in low-level debugging and performance analysis of programs and even state recovery of crashed applications. This papers documents the underlying design concept of Intel PT, the requirements on the Linux platform as well as some of its performance measurement use-cases.
Organization: | Nokia Solutions and Networks GmbH & Co. KG
Date: | August 2017
License: | [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
Download: | [Henschel_Intel-PT_2017.pdf](./Henschel_Intel-PT_2017.pdf)

## Quantitative Aspects of Blockchain: Proof Of Work

Title: | Quantitative Aspects of the Blockchain: Proof Of Work, its Energy Demand and Alternative Consensus Mechanism
 --- | ---
Abstract: | Bitcoin is the most popular and well-known cryptocurrency to date. It is built upon a technology called blockchain and utilizes a distributed consensus mechanism. This Proof Of Work consensus algorithm is very energy intensive and reports of Bitcoin’s growing energy usage are all over the news. In this paper we review the underlying blockchain technology, quantitative figures of the Bitcoin and Ethereum network and how they compare to each other. We also investigate the concepts of some alternative consensus mechanisms. Finally, we conclude that while the energy usage of Proof Of Work is very large and ever increasing, it opens up possibilities for new and previously unheard of applications. Furthermore, the elegance of Proof Of Work lies within its simplicity. In the future, however, other consensus algorithms might prevail, since Proof Of Work is only the first major iteration for the blockchain technology.
Organization: | University of Erlangen, Department of Computer Science
Date: | February 2018
License: | [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
Download: | [Henschel_BlockchainPOW_2018.pdf](./Henschel_BlockchainPOW_2018.pdf) ([Presentation Slides](./Henschel_BlockchainPOW-presentation_2017.pdf))

## PC-Netzteil Umbau

Title: | PC-Netzteil als Labor-Stromversorgung
 --- | ---
Abstract: | Auch bei uns stapeln sich ältere und inzwischen zu leistungsschwache PC-Netzteile im Keller. Statt sie der Entsorgung zuzuführen, empfehlen wir einen Umbau zum Labor-Netzteil - denn so etwas braucht man immer mal.
Journal: | c't Hacks / Make, Volume 5, Pages 82-97
Date: | 2015
Download: | [heise archive (free)](https://shop.heise.de/katalog/pc-netzteil-als-labor-stromversorgung)
