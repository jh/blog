#!/bin/sh

set -e


rm -rf public/
podman container run --rm -v "${PWD}:/blog" -w "/blog" docker.io/monachus/hugo:v0.75.1 hugo --log --minify

# First need to set up a remote with `rclone config create cubieserver-s3 s3 --all`
rclone sync --no-update-modtime public/ cubieserver-s3:blog-cubieserver-de/
