<!DOCTYPE html>
<html>
  <head>
    <title>PHP & HTML</title>
    <meta charset="utf-8">
    <style>
      body { font-family: 'Noto Serif', serif; }
      h1, h2, h3 {
        font-family: 'Linux Libertine', serif;
        font-weight: normal;
      }
      .remark-code, .remark-inline-code { font-family: 'Source Code Pro', monospace; font-size: 0.8em; }
    </style>
  </head>
  <body>
    <textarea id="source">

class: center, middle

# PHP & HTML

SDU, Internet Technology, 2019-06-21

*Jack Henschel*

---

## HTML

Hyper Text Markup Language

Enables creating structured documents by denoting structural semantics for text such as headings, paragraphs, lists, quotes ...

Consists of elements and attributes:

```html
<element attribute="value">
  Content
</element>
```

The DOM (Document Object Model) represents the logical tree by which HTML elements are connected:
```
            <html>
            /   \
      <head>    <body>
     /          /  |  \
<title>   <main> <nav> <footer>
            |
 ...       <p>     ...
```

???
Similar to XML (extensible markup language)

`<html>` is the root of the tree, all other elements are stored as childs

---

### Forms and Inputs

Used to collect user input in a structured manner

Consist of one or multiple Input elements which can have different types:
* Text
* Password
* Submit
* Checkbox
* Dropdown (Select)
* ... many more

Inputs have different attributes to control their behavior: `required`, `placeholder`, `range` ...
```html
<form action="user.php" method="POST">
    <input type="text" placeholder="Username" required/>
    <input type="password" required/>
    <input type="file" name="file"/>
    <input type="submit"/>
</form>
```

???
This will later be important when we talk about PHP

When sending a form with the submit button, the input fields and their values will be transferred to the server

PHP will then automatically pick up these values for us

---

## PHP
PHP: Hypertext Preprocessor is a general purpose programming language mainly focused on web development

Runs on the server (backend) and creates the content (HTML) that gets sent to the client

&rarr; Client never sees PHP source code!

```php
<h1><?php echo 'Hello World'; ?></h1>
```

Client HTML:
```html
<h1>Hello World</h1>
```

---

### Variables
Like Javascript, PHP is dynamically typed. Unlike JS, variables are denoted with a dollar sign `$`.

Scalar Data Types:
* Integer: represents non-fractional numeric values
* Float/Double: represents fractions
* String: Sequence of bytes, double quotes supports variable interpolation.
* Boolean: contains case insensitive TRUE or FALSE values

Compound Data Types:
* Array: associates value to keys
* Object: element derived from a class

Special Data Types:
* Resource: used to refer external resource data (file resource, database resource ...)
* NULL: case-insensitive PHP constant, represents no value

???

Eight primitive data types classified into 3 types:

Generally: PHP is pass-by-value

For primitives, copies are made

For objects, the value is a reference

**Scalar data types** contain only a single value

**Double data type** supports fractions with more decimal digits

**String** is enclosed by a pair of single or double quotes -> string interpolation

**Boolean**: Zero represents FALSE and any non-zero value represents boolean TRUE.

**Compound data type** (as per its name) will have a group of data, either of the same or of different types

**Array**: actually an ordered map. A map is a type that associates values to keys. Can be treated as an array, list (vector), hash table, dictionary, collection, stack, queue, and probably more. As array values can be other arrays, trees and multidimensional arrays are also possible. **More about this on the next slide**

**Objects**: have a collection of properties and functions (class methods) set with it

---


### PHP Arrays
Arrays in PHP can either be accessed via key-value pairs or via index:

```php
$array = array(
    "key" => "value",
    "foo" => 3,
    "x"
    );

echo $array['key']; // value
echo $array[2]; // x

$array[] = "y"; // add new item
$array["bar"] = 4; // add new key-value pair
unset($array["bar"]); // remove key
isset($array["bar"]); // check if element is present
print_r($array); // print out the entire array
```

---

### Functions

Does not support function overloading (function with same name but different signatures)
```php
function foobar($str) {
    echo 'Foo: ' . $str;
}

function foobar($str1, $str2) { // not allowed
    echo 'Foo: ' . $str1 . ' Bar: ' . $str2;
}
```

Supports default arguments for functions (some arguments may be left unspecified)
```php
function foobar($str1, $str2 = 'World') {
    echo 'Foo: ' . $str1 . ' Bar: ' . $str2;
}
foobar('Hello') // ok
```

Supports function overriding (function with same name and same signature replaces the implementation in parent class)

---

### Classes and Objects

Like most modern languages: Interfaces, Classes and Objects

```php
interface Person {
    public function __toString();
}

class Student {
    private $name;
    function __construct($name) {
		$this->name = $name;
	}
	function __destruct() {
		// destructor
	}
    function __toString() {
        return $this->name;
    }
}

$jack = new Student('jack');
echo $jack;
```

Garbage collection: For each object, PHP counts the references

When a reference is removed, and there are no more references to the object, the destructor is called

---

### Magic methods

Start with `__`, special functionality is associated with them

* `__construct` automatically runs when a new instance of the object is created

* `__destruct` is called when the reference counter hits zero (before the object is removed)

* `__toString` is called whenever a string representation of an object is required

Other magic methods in PHP classes:

`__call(), __callStatic(), __get(), __set(), __isset(), __unset(), __sleep(), __wakeup(), __invoke(), __set_state(), __clone(), __debugInfo()`

---

### Superglobals
Predefined Global Variables: "superglobals" (for communicating with the server application):

* `$GLOBALS`: lists all global variables

* `$_SERVER`: holds information about headers, paths, and script locations (`SERVER_NAME`, `HTTP_HOST`, ...)

* `$_ENV`: associative array of variables passed to the current script via the environment method

* `$_REQUEST`: associative array that by default contains the contents of `$_GET`, `$_POST` and `$_COOKIE`

* `$_POST`: collects form data after submitting an HTML form with method="post"

* `$_GET`: collects form data after submitting an HTML form with method="get"

* `$_FILES`: associative array of items uploaded to the current script via the HTTP POST method

* `$_COOKIE`: associative array of variables passed to the current script via HTTP Cookies

* `$_SESSION`: associative array containing session variables available to the current script

???

**super globals** are always accessible

---

### PHP Sessions
The superglobal `$_SESSION` contains information about a specific client.

PHP automatically sets a cookie to uniquely identify a client and select the right session.

*User does not have access to the data attached to a session*

```php
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

if ( isset($_SESSION["logged_in"] && $_SESSION["logged_in"] ) {
    echo 'Hello ' . $_SESSION["username"];
}
```

???

PHP has a really nice feature that makes it easy to identify users during a browsing session

this makes is super simple to implement a login state, for example

---

### User Input
Gets a specific external variable by name and filters it

```php
filter_input(int $type, string $variable_name, int $filter = FILTER_DEFAULT)

filter_input(INPUT_GET, "email", FILTER_VALIDATE_EMAIL)
```

Various filters available to validate and sanitize user input

### Combining different PHP scripts
Promotes code-reuse and helps keep the source code tidy

```php
include 'myFile.php'; // creates warning if it fails
include_once 'myFile.php'; // only includes if not included before
require 'myFile.php'; // throws fatal error if it fails
require_once 'myFile.php'; // only includes if not included before
```

???

User Input is always dangerous! Need to sanitize it

---

### Sources

* Lectures Slides "Internet Technology", Summer 2019, Henrik Lange, SDU
* W3Schools HTML & PHP References, https://www.w3schools.com/
* PHP Manual, The PHP Group, https://www.php.net/manual/en/

    </textarea>
    <script src="./remark-latest.min.js">
    </script>
    <script>
      var slideshow = remark.create({
          ratio: '16:9',
          highlightStyle: 'monokai',
      });
    </script>
  </body>
</html>
