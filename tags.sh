#!/bin/sh

# Prints out all currently used tags in the blog

find content/post/ -iname '*.md' -exec sed -nr 's/tags = \[(.+)\]/\1/p' '{}' + | sed 's/,/\n/g' | sed -nr 's/[[:space:]]*\"(.+)\"/\1/p' | sort -u
